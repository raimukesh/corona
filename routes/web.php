<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\ReportController;

Route::get('/', 'Auth\LoginController@showLoginForm');

Auth::routes(['verify' => true]);

Route::group(['middleware' => ['verified']], function () {
    Route::get('/home', 'HomeController@index')->name('home');

    Route::resource('officials', 'OfficialController');

    Route::get('individuals/normal', 'IndividualController@normal')->name('individuals.normal');
    Route::get('individuals/critical', 'IndividualController@critical')->name('individuals.critical');
    Route::resource('individuals', 'IndividualController');

    Route::resource('doctors', 'DoctorController');

    Route::resource('histories', 'HistoryController');

    Route::resource('symptoms', 'SymptomController');

    Route::get('/device_location', 'HomeController@deviceLocation')->name('deviceLocation');
});


Route::get('generator_builder', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@builder')->name('io_generator_builder');

Route::get('field_template', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@fieldTemplate')->name('io_field_template');

Route::get('relation_field_template', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@relationFieldTemplate')->name('io_relation_field_template');

Route::post('generator_builder/generate', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@generate')->name('io_generator_builder_generate');

Route::post('generator_builder/rollback', '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@rollback')->name('io_generator_builder_rollback');

Route::post(
    'generator_builder/generate-from-file',
    '\InfyOm\GeneratorBuilder\Controllers\GeneratorBuilderController@generateFromFile'
)->name('io_generator_builder_generate_from_file');


Route::resource('news', 'NewsController');

Route::resource('emergencyNumbers', 'EmergencyNumberController');


Route::resource('locations', 'LocationController');

Route::resource('announcements', 'AnnouncementController');

Route::resource('safeties', 'SafetyController');


// Official
Route::group(['prefix' => 'official', 'as' => 'official.'], function () {
    Route::get('/login', 'AuthOfficial\OfficialLoginController@showLoginForm')->name('login');
    Route::post('/login', 'AuthOfficial\OfficialLoginController@login')->name('login');
    Route::post('/logout', 'AuthOfficial\OfficialLoginController@logout')->name('logout');
});
Route::resource('nepalTrends', 'NepalTrendController');
Route::get('patient-report', 'ReportController@patientReport')->name('patientReport');
Route::get('profile', 'UserController@profile')->name('profile');
Route::get('users', 'UserController@index')->name('users.index');


Route::resource('meetings', 'MeetingController');

Route::resource('serverNumbers', 'ServerNumberController');

Route::resource('dangerZones', 'DangerZoneController');