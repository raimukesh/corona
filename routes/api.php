<?php

use Illuminate\Http\Request;
// use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// Login
Route::post('/login', 'AuthController@postLogin');
// Register
// Route::post('/register', 'AuthController@postRegister');



Route::get('individuals/suspect', 'IndividualAPIController@suspect');


Route::post('officials/register', 'OfficialAPIController@register');
Route::post('officials/login', 'OfficialAPIController@login');

Route::post('individuals/register', 'IndividualAPIController@register');
Route::post('individuals/login', 'IndividualAPIController@login');

Route::post('doctors/register', 'DoctorAPIController@register');
Route::post('doctors/login', 'DoctorAPIController@login');

Route::post('individuals/search_by_email', 'IndividualAPIController@searchByEmail');

// Protected with APIToken Middleware
Route::middleware('APIToken')->group(function () {

    Route::apiResource('histories', 'HistoryAPIController');
    Route::put('histories/review/approve/{history_id}/{review_id}', 'HistoryAPIController@approveReview')
        ->name('histories.review.approve');
    Route::get('histories/individual/{individual_id}', 'HistoryAPIController@historyByIndividual')->name('histories.individual');
    // Logout
    Route::post('/logout', 'AuthController@postLogout');
    Route::apiResource('officials', 'OfficialAPIController');
    Route::post('add/dependent', 'IndividualAPIController@addDependent');
    Route::get('individuals/get_by_location', 'IndividualAPIController@getByLocation');
    Route::get('individuals/dependent', 'IndividualAPIController@dependent');
    Route::get('individuals/search_mac_address', 'IndividualAPIController@searchMacAddress');
    Route::get('individuals/by_status/{status}', 'IndividualAPIController@byStatus')->name('individuals.by_status');
    Route::post('individuals/{id}', 'IndividualAPIController@update');
    Route::apiResource('individuals', 'IndividualAPIController');
    Route::resource('symptoms', 'SymptomAPIController');
    Route::resource('meetings', 'MeetingAPIController');
});

Route::apiResource('doctors', 'DoctorAPIController');
Route::resource('locations', 'LocationAPIController');


Route::get('email/verify/{id}/{userType}', 'VerificationApiController@verify')->name('verificationapi.verify');
Route::get('email/resend', 'VerificationApiController@resend')->name('verificationapi.resend');
Route::group(['middleware' => 'auth:api'], function () {
    Route::post('details', 'OfficialAPIController@details')->middleware('verified');
});


Route::resource('server_numbers', 'ServerNumberAPIController');
Route::resource('misscalls', 'MisscallAPIController');
Route::get('nepal_trends/today', 'NepalTrendAPIController@today');
Route::resource('nepal_trends', 'NepalTrendAPIController');
Route::resource('news', 'NewsAPIController');
Route::resource('announcements', 'AnnouncementAPIController');
Route::resource('emergency_numbers', 'EmergencyNumberAPIController');
Route::resource('safeties', 'SafetyAPIController');


Route::resource('danger_zones', 'DangerZoneAPIController');
