<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNepalTrendsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nepal_trends', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tested_positive');
            $table->integer('tested_negative');
            $table->integer('tested_total');
            $table->integer('in_isolation');
            $table->integer('recovered');
            $table->integer('deaths');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('nepal_trends');
    }
}
