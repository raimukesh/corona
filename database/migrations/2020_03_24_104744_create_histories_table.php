<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHistoriesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('histories', function (Blueprint $table) {
            $table->increments('id');
            $table->mediumText('symptoms')->nullable();
            $table->integer('individual_id')->unsigned();
            $table->longText('review')->nullable();
            $table->integer('approved_review_count')->unsigned()->default('0');
            $table->smallInteger('admin_comment')->nullable();
            $table->timestamps();
            $table->foreign('individual_id')->references('id')->on('individuals');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('histories');
    }
}
