<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Meeting;
use Faker\Generator as Faker;

$factory->define(Meeting::class, function (Faker $faker) {
    return [
        'individual_id' => $faker->randomDigitNotNull,
        'rssi' => $faker->randomDigitNotNull,
        'duration' => $faker->randomDigitNotNull,
        'distance' => $faker->randomDigitNotNull,
        'friend_mac_address'  => implode(':', str_split(str_pad(base_convert(mt_rand(0, 0xffffff), 10, 16) . base_convert(mt_rand(0, 0xffffff), 10, 16), 12), 2)),
        'latitude'  => mt_rand(260000,339999)/10000,
        'longitude'  => mt_rand(800000,869999)/10000,
        'date' => $faker->date('Y-m-d H:i:s'),
    ];
});
