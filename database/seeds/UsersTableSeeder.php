<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Sanam Shrestha',
            'phone' => '9841325684',
            'logo' => 'default.png',
            'address' => 'Lazimpat, Kathmandu',
            'ward' => 1,
            'municipality' => 'Budhanilkantha',
            'district' => 'Kathmandu',
            'province' => 'Bagmati',
            'email' => 'shresthacharitra5@gmail.com',
            'level' => 'admin',
            'password' => bcrypt('12345678'),
            'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),
            'email_verified_at' => \Carbon\Carbon::now()->format('Y-m-d H:i:s'),

        ]);
    }
}
