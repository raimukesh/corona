<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Auth;

class UserController extends Controller
{
    public function index()
    {
        $users = User::all();
        return view('users.index', compact('users'));
    }
    public function profile()
    {
        $user = User::find(Auth::user()->id);
        return view('users.profile', compact('user'));
    }
}
