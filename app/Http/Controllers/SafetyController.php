<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSafetyRequest;
use App\Http\Requests\UpdateSafetyRequest;
use App\Repositories\SafetyRepository;
use App\Http\Controllers\AppBaseController;
use App\Models\Safety;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Flash;
use Response;

class SafetyController extends AppBaseController
{
    /** @var  SafetyRepository */
    private $safetyRepository;

    public function __construct(SafetyRepository $safetyRepo)
    {
        $this->safetyRepository = $safetyRepo;
    }

    /**
     * Display a listing of the Safety.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $safeties = Safety::orderBy('id', 'desc')->get();

        return view('safeties.index')
            ->with('safeties', $safeties);
    }

    /**
     * Show the form for creating a new Safety.
     *
     * @return Response
     */
    public function create()
    {
        return view('safeties.create');
    }

    /**
     * Store a newly created Safety in storage.
     *
     * @param CreateSafetyRequest $request
     *
     * @return Response
     */
    public function store(CreateSafetyRequest $request)
    {
        $input = $request->all();

        $safety = $this->safetyRepository->create($input);

        Flash::success('Safety saved successfully.');

        return redirect(route('safeties.index'));
    }

    /**
     * Display the specified Safety.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $safety = $this->safetyRepository->find($id);

        if (empty($safety)) {
            Flash::error('Safety not found');

            return redirect(route('safeties.index'));
        }

        return view('safeties.show')->with('safety', $safety);
    }

    /**
     * Show the form for editing the specified Safety.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $safety = $this->safetyRepository->find($id);

        if (empty($safety)) {
            Flash::error('Safety not found');

            return redirect(route('safeties.index'));
        }

        return view('safeties.edit')->with('safety', $safety);
    }

    /**
     * Update the specified Safety in storage.
     *
     * @param int $id
     * @param UpdateSafetyRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSafetyRequest $request)
    {
        $safety = $this->safetyRepository->find($id);

        if (empty($safety)) {
            Flash::error('Safety not found');

            return redirect(route('safeties.index'));
        }

        $safety = $this->safetyRepository->update($request->all(), $id);

        Flash::success('Safety updated successfully.');

        return redirect(route('safeties.index'));
    }

    /**
     * Remove the specified Safety from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $safety = $this->safetyRepository->find($id);

        if (empty($safety)) {
            Flash::error('Safety not found');

            return redirect(route('safeties.index'));
        }

        $this->safetyRepository->delete($id);

        Flash::success('Safety deleted successfully.');

        return redirect(route('safeties.index'));
    }

    public function checkGuard()
    {
        if (Auth::guard()->check()) {
            $path = "";
            return $path;
        } elseif (Auth::guard('official')->check()) {
            $path = "auth_official.pages.";
            return $path;
        }
    }
}
