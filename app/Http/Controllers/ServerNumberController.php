<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateServerNumberRequest;
use App\Http\Requests\UpdateServerNumberRequest;
use App\Repositories\ServerNumberRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class ServerNumberController extends AppBaseController
{
    /** @var  ServerNumberRepository */
    private $serverNumberRepository;

    public function __construct(ServerNumberRepository $serverNumberRepo)
    {
        $this->serverNumberRepository = $serverNumberRepo;
    }

    /**
     * Display a listing of the ServerNumber.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $serverNumbers = $this->serverNumberRepository->paginate(10);

        return view('server_numbers.index')
            ->with('serverNumbers', $serverNumbers);
    }

    /**
     * Show the form for creating a new ServerNumber.
     *
     * @return Response
     */
    public function create()
    {
        return view('server_numbers.create');
    }

    /**
     * Store a newly created ServerNumber in storage.
     *
     * @param CreateServerNumberRequest $request
     *
     * @return Response
     */
    public function store(CreateServerNumberRequest $request)
    {
        $input = $request->all();

        $serverNumber = $this->serverNumberRepository->create($input);

        Flash::success('Server Number saved successfully.');

        return redirect(route('serverNumbers.index'));
    }

    /**
     * Display the specified ServerNumber.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $serverNumber = $this->serverNumberRepository->find($id);

        if (empty($serverNumber)) {
            Flash::error('Server Number not found');

            return redirect(route('serverNumbers.index'));
        }

        return view('server_numbers.show')->with('serverNumber', $serverNumber);
    }

    /**
     * Show the form for editing the specified ServerNumber.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $serverNumber = $this->serverNumberRepository->find($id);

        if (empty($serverNumber)) {
            Flash::error('Server Number not found');

            return redirect(route('serverNumbers.index'));
        }

        return view('server_numbers.edit')->with('serverNumber', $serverNumber);
    }

    /**
     * Update the specified ServerNumber in storage.
     *
     * @param int $id
     * @param UpdateServerNumberRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateServerNumberRequest $request)
    {
        $serverNumber = $this->serverNumberRepository->find($id);

        if (empty($serverNumber)) {
            Flash::error('Server Number not found');

            return redirect(route('serverNumbers.index'));
        }

        $serverNumber = $this->serverNumberRepository->update($request->all(), $id);

        Flash::success('Server Number updated successfully.');

        return redirect(route('serverNumbers.index'));
    }

    /**
     * Remove the specified ServerNumber from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $serverNumber = $this->serverNumberRepository->find($id);

        if (empty($serverNumber)) {
            Flash::error('Server Number not found');

            return redirect(route('serverNumbers.index'));
        }

        $this->serverNumberRepository->delete($id);

        Flash::success('Server Number deleted successfully.');

        return redirect(route('serverNumbers.index'));
    }
}
