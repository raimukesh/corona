<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateNewsRequest;
use App\Http\Requests\UpdateNewsRequest;
use App\Repositories\NewsRepository;
use App\Http\Controllers\AppBaseController;
use App\Models\News;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Flash;
use Response;

class NewsController extends AppBaseController
{
    /** @var  NewsRepository */
    private $newsRepository;

    public function __construct(NewsRepository $newsRepo)
    {
        $this->newsRepository = $newsRepo;
    }

    /**
     * Display a listing of the News.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $news = News::orderBy('id', 'desc')->get();

        return view('news.index')
            ->with('news', $news);
    }

    /**
     * Show the form for creating a new News.
     *
     * @return Response
     */
    public function create()
    {
        return view('news.create');
    }

    /**
     * Store a newly created News in storage.
     *
     * @param CreateNewsRequest $request
     *
     * @return Response
     */
    public function store(CreateNewsRequest $request)
    {
        $input = $request->all();
        if ($request->hasFile('thumbnail')) {
            $file = $request->file('thumbnail');
            $fileNameWithExt = $request->file('thumbnail')->getClientOriginalName();
            $filename_temp = preg_replace('/[^a-zA-Z0-9_-]+/', '', pathinfo($fileNameWithExt, PATHINFO_FILENAME));
            $extension = $request->file('thumbnail')->getClientOriginalExtension();
            $filename = $filename_temp . '_' . time() . '.' . $extension;
            $input['thumbnail'] = 'storage/news/' . $filename;
            $path = $file->storeAs('news', $filename, 'public');
        }
        $news = $this->newsRepository->create($input);

        Flash::success('News saved successfully.');

        return redirect(route('news.index'));
    }

    /**
     * Display the specified News.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $news = $this->newsRepository->find($id);

        if (empty($news)) {
            Flash::error('News not found');

            return redirect(route('news.index'));
        }

        return view('news.show')->with('news', $news);
    }

    /**
     * Show the form for editing the specified News.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $news = $this->newsRepository->find($id);

        if (empty($news)) {
            Flash::error('News not found');

            return redirect(route('news.index'));
        }

        return view('news.edit')->with('news', $news);
    }

    /**
     * Update the specified News in storage.
     *
     * @param int $id
     * @param UpdateNewsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateNewsRequest $request)
    {
        $news = $this->newsRepository->find($id);

        if (empty($news)) {
            Flash::error('News not found');

            return redirect(route('news.index'));
        }
        $input = $request->all();
        if ($request->hasFile('thumbnail')) {
            $file = $request->file('thumbnail');
            $fileNameWithExt = $request->file('thumbnail')->getClientOriginalName();
            $filename_temp = preg_replace('/[^a-zA-Z0-9_-]+/', '', pathinfo($fileNameWithExt, PATHINFO_FILENAME));
            $extension = $request->file('thumbnail')->getClientOriginalExtension();
            $filename = $filename_temp . '_' . time() . '.' . $extension;
            $input['thumbnail'] = 'storage/news/' . $filename;
            $path = $file->storeAs('news', $filename, 'public');
        }

        $news = $this->newsRepository->update($input, $id);

        Flash::success('News updated successfully.');

        return redirect(route('news.index'));
    }

    /**
     * Remove the specified News from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $news = $this->newsRepository->find($id);

        if (empty($news)) {
            Flash::error('News not found');

            return redirect(route('news.index'));
        }

        $this->newsRepository->delete($id);

        Flash::success('News deleted successfully.');

        return redirect(route('news.index'));
    }

    public function checkGuard()
    {
        if (Auth::guard()->check()) {
            $path = "";
            return $path;
        } elseif (Auth::guard('official')->check()) {
            $path = "auth_official.pages.";
            return $path;
        }
    }
}
