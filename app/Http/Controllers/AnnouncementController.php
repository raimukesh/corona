<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateAnnouncementRequest;
use App\Http\Requests\UpdateAnnouncementRequest;
use App\Repositories\AnnouncementRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Flash;
use Response;

class AnnouncementController extends AppBaseController
{
    /** @var  AnnouncementRepository */
    private $announcementRepository;

    public function __construct(AnnouncementRepository $announcementRepo)
    {
        $this->announcementRepository = $announcementRepo;
    }

    /**
     * Display a listing of the Announcement.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $announcements = $this->announcementRepository->paginate(10);
        $path = $this->checkGuard();

        return view($path . 'announcements.index')
            ->with('announcements', $announcements);
    }

    /**
     * Show the form for creating a new Announcement.
     *
     * @return Response
     */
    public function create()
    {
        return view('announcements.create');
    }

    /**
     * Store a newly created Announcement in storage.
     *
     * @param CreateAnnouncementRequest $request
     *
     * @return Response
     */
    public function store(CreateAnnouncementRequest $request)
    {
        $input = $request->all();
        if ($request->hasFile('thumbnail')) {
            $file = $request->file('thumbnail');
            $fileNameWithExt = $request->file('thumbnail')->getClientOriginalName();
            $filename_temp = preg_replace('/[^a-zA-Z0-9_-]+/', '', pathinfo($fileNameWithExt, PATHINFO_FILENAME));
            $extension = $request->file('thumbnail')->getClientOriginalExtension();
            $filename = $filename_temp . '_' . time() . '.' . $extension;
            $input['thumbnail'] = 'storage/announcements/' . $filename;
            $path = $file->storeAs('announcements', $filename, 'public');
        }

        $announcement = $this->announcementRepository->create($input);

        Flash::success('Announcement saved successfully.');

        return redirect(route('announcements.index'));
    }

    /**
     * Display the specified Announcement.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $announcement = $this->announcementRepository->find($id);

        if (empty($announcement)) {
            Flash::error('Announcement not found');

            return redirect(route('announcements.index'));
        }

        return view('announcements.show')->with('announcement', $announcement);
    }

    /**
     * Show the form for editing the specified Announcement.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $announcement = $this->announcementRepository->find($id);

        if (empty($announcement)) {
            Flash::error('Announcement not found');

            return redirect(route('announcements.index'));
        }

        return view('announcements.edit')->with('announcement', $announcement);
    }

    /**
     * Update the specified Announcement in storage.
     *
     * @param int $id
     * @param UpdateAnnouncementRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAnnouncementRequest $request)
    {
        $announcement = $this->announcementRepository->find($id);

        if (empty($announcement)) {
            Flash::error('Announcement not found');

            return redirect(route('announcements.index'));
        }
        $input = $request->all();
        if ($request->hasFile('thumbnail')) {
            $file = $request->file('thumbnail');
            $fileNameWithExt = $request->file('thumbnail')->getClientOriginalName();
            $filename_temp = preg_replace('/[^a-zA-Z0-9_-]+/', '', pathinfo($fileNameWithExt, PATHINFO_FILENAME));
            $extension = $request->file('thumbnail')->getClientOriginalExtension();
            $filename = $filename_temp . '_' . time() . '.' . $extension;
            $input['thumbnail'] = 'storage/announcements/' . $filename;
            $path = $file->storeAs('announcements', $filename, 'public');
        }

        $announcement = $this->announcementRepository->update($input, $id);

        Flash::success('Announcement updated successfully.');

        return redirect(route('announcements.index'));
    }

    /**
     * Remove the specified Announcement from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $announcement = $this->announcementRepository->find($id);

        if (empty($announcement)) {
            Flash::error('Announcement not found');

            return redirect(route('announcements.index'));
        }

        $this->announcementRepository->delete($id);

        Flash::success('Announcement deleted successfully.');

        return redirect(route('announcements.index'));
    }
    public function checkGuard()
    {
        if (Auth::guard()->check()) {
            $path = "";
            return $path;
        } elseif (Auth::guard('official')->check()) {
            $path = "auth_official.pages.";
            return $path;
        }
    }
}
