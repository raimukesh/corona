<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateMeetingAPIRequest;
use App\Http\Requests\API\UpdateMeetingAPIRequest;
use App\Models\Meeting;
use App\Repositories\MeetingRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Models\Individual;
use Carbon\Carbon;
use Response;

/**
 * Class MeetingController
 * @package App\Http\Controllers\API
 */

class MeetingAPIController extends AppBaseController
{
    /** @var  MeetingRepository */
    private $meetingRepository;

    public function __construct(MeetingRepository $meetingRepo)
    {
        $this->meetingRepository = $meetingRepo;
    }

    /**
     * Display a listing of the Meeting.
     * GET|HEAD /meetings
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $meetings = $this->meetingRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($meetings->toArray(), 'Meetings retrieved successfully');
    }

    /**
     * Store a newly created Meeting in storage.
     * POST /meetings
     *
     * @param CreateMeetingAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateMeetingAPIRequest $request)
    {
        $input = $request->all();
        $meeting = $this->meetingRepository->create($input);
        return $this->sendResponse($meeting->toArray(), 'Meeting saved successfully');
    }

    /**
     * Display the specified Meeting.
     * GET|HEAD /meetings/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Meeting $meeting */
        $meeting = $this->meetingRepository->find($id);

        if (empty($meeting)) {
            return $this->sendError('Meeting not found');
        }

        return $this->sendResponse($meeting->toArray(), 'Meeting retrieved successfully');
    }

    /**
     * Update the specified Meeting in storage.
     * PUT/PATCH /meetings/{id}
     *
     * @param int $id
     * @param UpdateMeetingAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMeetingAPIRequest $request)
    {
        $input = $request->all();

        /** @var Meeting $meeting */
        $meeting = $this->meetingRepository->find($id);

        if (empty($meeting)) {
            return $this->sendError('Meeting not found');
        }

        $meeting = $this->meetingRepository->update($input, $id);

        return $this->sendResponse($meeting->toArray(), 'Meeting updated successfully');
    }

    /**
     * Remove the specified Meeting from storage.
     * DELETE /meetings/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Meeting $meeting */
        $meeting = $this->meetingRepository->find($id);

        if (empty($meeting)) {
            return $this->sendError('Meeting not found');
        }

        $meeting->delete();

        return $this->sendSuccess('Meeting deleted successfully');
    }
}
