<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateServerNumberAPIRequest;
use App\Http\Requests\API\UpdateServerNumberAPIRequest;
use App\Models\ServerNumber;
use App\Repositories\ServerNumberRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class ServerNumberController
 * @package App\Http\Controllers\API
 */

class ServerNumberAPIController extends AppBaseController
{
    /** @var  ServerNumberRepository */
    private $serverNumberRepository;

    public function __construct(ServerNumberRepository $serverNumberRepo)
    {
        $this->serverNumberRepository = $serverNumberRepo;
    }

    /**
     * Display a listing of the ServerNumber.
     * GET|HEAD /serverNumbers
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $serverNumbers = $this->serverNumberRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($serverNumbers->toArray(), 'Server Numbers retrieved successfully');
    }

    /**
     * Store a newly created ServerNumber in storage.
     * POST /serverNumbers
     *
     * @param CreateServerNumberAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateServerNumberAPIRequest $request)
    {
        $input = $request->all();

        $serverNumber = $this->serverNumberRepository->create($input);

        return $this->sendResponse($serverNumber->toArray(), 'Server Number saved successfully');
    }

    /**
     * Display the specified ServerNumber.
     * GET|HEAD /serverNumbers/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var ServerNumber $serverNumber */
        $serverNumber = $this->serverNumberRepository->find($id);

        if (empty($serverNumber)) {
            return $this->sendError('Server Number not found');
        }

        return $this->sendResponse($serverNumber->toArray(), 'Server Number retrieved successfully');
    }

    /**
     * Update the specified ServerNumber in storage.
     * PUT/PATCH /serverNumbers/{id}
     *
     * @param int $id
     * @param UpdateServerNumberAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateServerNumberAPIRequest $request)
    {
        $input = $request->all();

        /** @var ServerNumber $serverNumber */
        $serverNumber = $this->serverNumberRepository->find($id);

        if (empty($serverNumber)) {
            return $this->sendError('Server Number not found');
        }

        $serverNumber = $this->serverNumberRepository->update($input, $id);

        return $this->sendResponse($serverNumber->toArray(), 'ServerNumber updated successfully');
    }

    /**
     * Remove the specified ServerNumber from storage.
     * DELETE /serverNumbers/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var ServerNumber $serverNumber */
        $serverNumber = $this->serverNumberRepository->find($id);

        if (empty($serverNumber)) {
            return $this->sendError('Server Number not found');
        }

        $serverNumber->delete();

        return $this->sendSuccess('Server Number deleted successfully');
    }
}
