<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Official;
use App\Repositories\OfficialRepository;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use Auth;
use Validator;
use Illuminate\Foundation\Auth\VerifiesEmails;
use Illuminate\Support\Str;

class OfficialAPIController extends Controller
{
    use VerifiesEmails;
    public $successStatus = 200;
    private $individualRepository;

    public function __construct(OfficialRepository $officialRepo)
    {
        $this->officialRepository = $officialRepo;
    }
    /**
     * login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        // Validations
        $rules = [
            'email' => 'required|email',
            'password' => 'required|min:8'
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            // Validation failed
            return response()->json([
                'message' => $validator->messages(),
            ]);
        } else {
            // Fetch Client
            $client = User::where('email', $request->email)->first();
            if ($client->email_verified_at == null || empty($client->email_verified_at)) {
                return response()->json([
                    'message' => 'Email not verified',
                ]);
            }
            if ($client) {
                // Verify the password
                if (password_verify($request->password, $client->password)) {
                    // Update Token
                    $postArray = ['api_token' => uniqid(base64_encode(Str::random(40)))];
                    $login = User::where('email', $request->email)->update($postArray);

                    if ($login) {
                        $client['api_token'] = $postArray['api_token'];
                        return $this->sendResponse($client->toArray(), 'Official login successfully');
                    }
                } else {
                    return response()->json([
                        'message' => 'Invalid Password',
                    ]);
                }
            } else {
                return response()->json([
                    'message' => 'Client not found',
                ]);
            }
        }
    }
    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            // 'c_password' => 'required|same:password',
        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }
        $input = $request->all();
        $official = User::where('email', $input['email'])->first();
        if ($official) {
            return $this->sendError('Email has been already registered');
        }
        $input['password'] = Hash::make($input['password']);
        $user = User::create($input);
        $user->sendApiEmailVerificationNotification();
        return $this->sendSuccess('Please confirm yourself by clicking on verify user button sent to you on your email');
    }

    public function index(Request $request)
    {
        $individuals = $this->officialRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($individuals->toArray(), 'Officials retrieved successfully');
    }
    /**
     * details api
     *
     * @return \Illuminate\Http\Response
     */
    public function details()
    {
        $user = Auth::user();
        return response()->json(['success' => $user], $this->successStatus);
    }

    public function show($id)
    {
        $official = User::find($id);

        if (empty($official)) {
            return $this->sendError('Official not found');
        }
        return $this->sendResponse($official->toArray(), 'Official retrieved successfully');
    }
}
