<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateEmergencyNumberAPIRequest;
use App\Http\Requests\API\UpdateEmergencyNumberAPIRequest;
use App\Models\EmergencyNumber;
use App\Repositories\EmergencyNumberRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class EmergencyNumberController
 * @package App\Http\Controllers\API
 */

class EmergencyNumberAPIController extends AppBaseController
{
    /** @var  EmergencyNumberRepository */
    private $emergencyNumberRepository;

    public function __construct(EmergencyNumberRepository $emergencyNumberRepo)
    {
        $this->emergencyNumberRepository = $emergencyNumberRepo;
    }

    /**
     * Display a listing of the EmergencyNumber.
     * GET|HEAD /emergencyNumbers
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $emergencyNumbers = EmergencyNumber::orderBy('id', 'desc')->get();

        return $this->sendResponse($emergencyNumbers->toArray(), 'Emergency Numbers retrieved successfully');
    }

    /**
     * Store a newly created EmergencyNumber in storage.
     * POST /emergencyNumbers
     *
     * @param CreateEmergencyNumberAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateEmergencyNumberAPIRequest $request)
    {
        $input = $request->all();

        $emergencyNumber = $this->emergencyNumberRepository->create($input);

        return $this->sendResponse($emergencyNumber->toArray(), 'Emergency Number saved successfully');
    }

    /**
     * Display the specified EmergencyNumber.
     * GET|HEAD /emergencyNumbers/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var EmergencyNumber $emergencyNumber */
        $emergencyNumber = $this->emergencyNumberRepository->find($id);

        if (empty($emergencyNumber)) {
            return $this->sendError('Emergency Number not found');
        }

        return $this->sendResponse($emergencyNumber->toArray(), 'Emergency Number retrieved successfully');
    }

    /**
     * Update the specified EmergencyNumber in storage.
     * PUT/PATCH /emergencyNumbers/{id}
     *
     * @param int $id
     * @param UpdateEmergencyNumberAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateEmergencyNumberAPIRequest $request)
    {
        $input = $request->all();

        /** @var EmergencyNumber $emergencyNumber */
        $emergencyNumber = $this->emergencyNumberRepository->find($id);

        if (empty($emergencyNumber)) {
            return $this->sendError('Emergency Number not found');
        }

        $emergencyNumber = $this->emergencyNumberRepository->update($input, $id);

        return $this->sendResponse($emergencyNumber->toArray(), 'EmergencyNumber updated successfully');
    }

    /**
     * Remove the specified EmergencyNumber from storage.
     * DELETE /emergencyNumbers/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var EmergencyNumber $emergencyNumber */
        $emergencyNumber = $this->emergencyNumberRepository->find($id);

        if (empty($emergencyNumber)) {
            return $this->sendError('Emergency Number not found');
        }

        $emergencyNumber->delete();

        return $this->sendSuccess('Emergency Number deleted successfully');
    }
}
