<?php

namespace App\Http\Controllers\API;

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use App\Http\Requests\API\CreateHistoryAPIRequest;
use App\Http\Requests\API\UpdateHistoryAPIRequest;
use App\Models\History;
use App\Repositories\HistoryRepository;
use App\Http\Controllers\AppBaseController;
use App\Models\Doctor;
use Carbon\Carbon;
use Response;

/**
 * Class HistoryController
 * @package App\Http\Controllers\API
 */

class HistoryAPIController extends AppBaseController
{
    /** @var  HistoryRepository */
    private $historyRepository;

    public function __construct(HistoryRepository $historyRepo)
    {
        $this->historyRepository = $historyRepo;
    }

    /**
     * Display a listing of the History.
     * GET|HEAD /histories
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {

        $input = $request->all();

        $histories = '';
        $responseMsg = 'Histories retrieved successfully';
        if ($request->all()['user_type'] == 'individuals') {
            $histories = History::where('individual_id', $input['individual_id'])->with('individual')->orderBy('id', 'desc')->paginate(15);
        } elseif ($input['user_type'] == 'doctors' || $input['user_type'] == 'officials') {
            if (isset($input['fetch_type'])) {
                if ($input['fetch_type'] == 'random') {

                    $histories = History::with('individual')->orderBy('approved_review_count', 'asc')->inRandomOrder()->paginate(15);
                    $responseMsg = 'Random Histories retrieved successfully';
                } else {
                    $histories = History::orderBy('id', 'desc')->with('individual')->paginate(15);
                }
            } else {
                $histories = History::orderBy('id', 'desc')->with('individual')->paginate(15);
            }
        }

        return $this->sendResponse($histories->toArray(), $responseMsg);
    }

    /**
     * Store a newly created History in storage.
     * POST /histories
     *
     * @param CreateHistoryAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateHistoryAPIRequest $request)
    {
        $input = $request->all();

        $history = $this->historyRepository->create($input);

        return $this->sendResponse($history->toArray(), 'History saved successfully');
    }

    /**
     * Display the specified History.
     * GET|HEAD /histories/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var History $history */
        $history = $this->historyRepository->find($id);

        if (empty($history)) {
            return $this->sendError('History not found');
        }

        return $this->sendResponse($history->toArray(), 'History retrieved successfully');
    }

    public function historyByIndividual($id)
    {
        $history = History::where('individual_id', $id)->orderBy('id', 'desc')->paginate(15);

        if (empty($history)) {
            return $this->sendError('History not found');
        }

        return $this->sendResponse($history->toArray(), 'History for specific individual retrieved successfully');
    }

    /**
     * Update the specified History in storage.
     * PUT/PATCH /histories/{id}
     *
     * @param int $id
     * @param UpdateHistoryAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateHistoryAPIRequest $request)
    {
        $input = $request->all();

        $myObj = [];
        $myObj['review'] = $input['review'];
        $myObj['doctor_id'] = (int) $input['doctor_id'];
        $myObj['doctor_name'] = Doctor::find($input['doctor_id'])->name;
        if (Doctor::find($input['doctor_id'])->level == "Expert") {
            // add approved_review_count by 1
            $add = 1;
            $myObj['approved'] = 1;
            $myObj['approved_by'] = $myObj['doctor_name'];
        } else {
            $add = 0;
            $myObj['approved'] = 0;
            $myObj['approved_by'] = null;
        }
        $myObj['created_at'] = date_format(Carbon::now(), "Y-m-d H:i:s");
        /** @var History $history */
        $history = $this->historyRepository->find($id);
        $reviews = json_decode($history->review);
        $myObj['review_id'] = sizeof($reviews) + 1;
        array_push($reviews, $myObj);
        $input['review'] = json_encode($reviews);
        $input['approved_review_count'] = $history->approved_review_count + $add;

        if (empty($history)) {
            return $this->sendError('History not found');
        }

        $history = $this->historyRepository->update($input, $id);

        return $this->sendResponse($history->toArray(), 'History updated successfully');
    }

    /**
     * Remove the specified History from storage.
     * DELETE /histories/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var History $history */
        $history = $this->historyRepository->find($id);

        if (empty($history)) {
            return $this->sendError('History not found');
        }

        $history->delete();

        return $this->sendSuccess('History deleted successfully');
    }

    public function approveReview($history_id, $review_id, Request $request)
    {

        $history = $this->historyRepository->find($history_id);
        // dd(empty($history),empty($history->review));
        if (empty($history)) {
            return $this->sendError('History not found');
        }
        $reviews = json_decode($history->review);
        if (empty($reviews) || $reviews == []) {
            return $this->sendError('Review Empty');
        }

        foreach ($reviews as $review) {
            if ($review->review_id == $review_id) {
                $review->approved = 1;
                $review->approved_by = Doctor::where('api_token', $request->bearerToken())->first()->name;
                break;
            }
        }
        $history['review'] = json_encode($reviews);
        $history['approved_review_count'] = ++$history->approved_review_count;

        // dd($history);
        $history = $this->historyRepository->update($history->toArray(), $history_id);

        return $this->sendResponse($history->toArray(), 'Review approved successfully');
    }
}
