<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Client;
use DB;
use Validator;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    private $apiToken;
    public function __construct()
    {
        // Unique Token
        $this->apiToken = uniqid(base64_encode(Str::random(40)));
    }
    /**
     * Client Login
     */
    public function postLogin(Request $request)
    {
        // Validations
        $rules = [
            'email' => 'required|email',
            'password' => 'required|min:8'
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            // Validation failed
            return response()->json([
                'message' => $validator->messages(),
            ]);
        } else {
            // Fetch Client
            $client = Client::where('email', $request->email)->first();
            if ($client) {
                // Verify the password
                if (password_verify($request->password, $client->password)) {
                    // Update Token
                    $postArray = ['api_token' => $this->apiToken];
                    $login = Client::where('email', $request->email)->update($postArray);

                    if ($login) {
                        // return response()->json([
                        //     'name'         => $client->name,
                        //     'email'        => $client->email,
                        //     'access_token' => $this->apiToken,
                        // ]);
                        return $this->sendResponse($client->toArray(), 'Clients retrieved successfully');
                    }
                } else {
                    return response()->json([
                        'message' => 'Invalid Password',
                    ]);
                }
            } else {
                return response()->json([
                    'message' => 'Client not found',
                ]);
            }
        }
    }
    /**
     * Register
     */
    // public function postRegister(Request $request)
    // {
    //     // Validations
    //     $rules = [
    //         'name'     => 'required|min:3',
    //         'email'    => 'required|unique:clients,email',
    //         'password' => 'required|min:8'
    //     ];
    //     $validator = Validator::make($request->all(), $rules);
    //     if ($validator->fails()) {
    //         // Validation failed
    //         return response()->json([
    //             'message' => $validator->messages(),
    //         ]);
    //     } else {
    //         $postArray = [
    //             'name'      => $request->name,
    //             'email'     => $request->email,
    //             'password'  => bcrypt($request->password),
    //             'api_token' => $this->apiToken
    //         ];
    //         // $client = Client::GetInsertId($postArray);
    //         $client = Client::insert($postArray);

    //         if ($client) {
    //             return response()->json([
    //                 'name'         => $request->name,
    //                 'email'        => $request->email,
    //                 'access_token' => $this->apiToken,
    //             ]);
    //         } else {
    //             return response()->json([
    //                 'message' => 'Registration failed, please try again.',
    //             ]);
    //         }
    //     }
    // }
    /**
     * Logout
     */
    public function postLogout(Request $request)
    {
        $token = $request->header('Authorization');
        $client = Client::where('api_token', $token)->first();
        if ($client) {
            $postArray = ['api_token' => null];
            $logout = Client::where('id', $client->id)->update($postArray);
            if ($logout) {
                return response()->json([
                    'message' => 'Client Logged Out',
                ]);
            }
        } else {
            return response()->json([
                'message' => 'Client not found',
            ]);
        }
    }
}
