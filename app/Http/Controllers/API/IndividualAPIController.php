<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateIndividualAPIRequest;
use App\Http\Requests\API\UpdateIndividualAPIRequest;
use App\Models\Individual;
use App\Repositories\IndividualRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Models\Meeting;
use App\Models\Misscall;
use App\Models\Official;
use App\User;
use Carbon\Carbon;
use DateTime;
use Illuminate\Foundation\Auth\VerifiesEmails;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Response;
use Validator;
use Illuminate\Support\Str;

/**
 * Class IndividualController
 * @package App\Http\Controllers\API
 */

class IndividualAPIController extends AppBaseController
{
    use VerifiesEmails;
    public $successStatus = 200;
    /** @var  IndividualRepository */
    private $individualRepository;

    public function __construct(IndividualRepository $individualRepo)
    {
        $this->individualRepository = $individualRepo;
    }
    public function login(Request $request)
    {
        // Validations
        $rules = [
            // 'password' => 'required|min:8'
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            // Validation failed
            return response()->json([
                'message' => $validator->messages(),
            ]);
        } else {
            $input = $request->all();
            // Check the miscall list
            $validMisscall = Misscall::where('number', $input['password'])->orderBy('id', 'desc')->first();
            if ($validMisscall) {
                if ($validMisscall->created_at->diffInMinutes(Carbon::now()) < 5) {
                    $client = Individual::where('phone', $input['password'])->first();
                    if ($client) {
                        // Update Token
                        $postArray = ['api_token' => uniqid(base64_encode(Str::random(40)))];
                        $login = Individual::where('phone', $request->password)->update($postArray);

                        if ($login) {
                            $client['api_token'] = $postArray['api_token'];
                            return $this->sendResponse($client->toArray(), 'Individual login successfully');
                        }
                    } else {
                        return $this->sendError('Number not registered');
                    }
                } else {
                    return $this->sendError('Session expired');
                }
            } else {
                return $this->sendError('Call record is not stored');
            }
        }
    }
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone' => 'required',
            'longitude' => 'required',
            'latitude' => 'required',
            'plus_code' => 'required',
            'password' => 'required',
            'mac_address' => 'required'


        ]);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }
        $input = $request->all();
        // Check if the user already exixts or not
        $individual = Individual::where('phone', $input['phone'])->first();
        if ($individual) {

            return $this->sendError('Individual already registered');
        }
        $validMisscall = Misscall::where('number', $input['phone'])->orderBy('id', 'desc')->first();
        if ($validMisscall) {
            if ($validMisscall->created_at->diffInMinutes(Carbon::now()) < 5) {

                if (array_key_exists('password', $input)) {
                    $input['password'] = Hash::make($input['password']);
                }
                $input['api_token'] = uniqid(base64_encode(Str::random(40)));

                $input['current_latitude'] = $input['latitude'];
                $input['current_longitude'] = $input['longitude'];
                $user = Individual::create($input);
                // if (array_key_exists('email', $input)) {
                //     $user->sendApiEmailVerificationNotification();

                //     $success['message'] = 'Please confirm yourself by clicking on verify user button sent to you on your email';
                //     return response()->json(['success' => $success], $this->successStatus);
                // }
                return $this->sendResponse(Individual::find($user->id)->toArray(), 'Individual login successfully');
            } else {
                return $this->sendError('Session expired');
            }
        } else {
            return $this->sendError('Call record is not stored');
        }
    }

    public function addDependent(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'age' => 'required',
            'gender' => 'required',
            'parent_id' => 'required',
        ]);


        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 401);
        }
        $input = $request->all();

        $parent = Individual::find($input['parent_id']);

        if ($parent != null) {
            $input['ward'] = $parent->ward;
            $input['municipality'] = $parent->municipality;
            $input['province'] = $parent->province;
            $input['address'] = $parent->address;
            $input['district'] = $parent->district;
            $input['longitude'] = $parent->longitude;
            $input['latitude'] = $parent->latitude;
            $input['plus_code'] = $parent->plus_code;
            // what about phone??
        } else {
            return $this->sendError('Parent Id not found.');
        }

        $user = Individual::create($input);

        return $this->sendResponse(Individual::find($user->id)->toArray(), 'Dependent Individual added successfully');
    }

    public function getByLocation(Request $request)
    {
        $input = $request->all();
        $lat = $input['current_latitude'];
        $lng = $input['current_longitude'];
        $range = $input['range'] ?? 20;
        if ($input['user_type'] == 'individuals') {

            $individual = Individual::find($input['client_id']);
            $individual->update([
                'current_latitude' => $lat,
                'current_longitude' => $lng,
            ]);
        }
        $individuals = Individual::all(['id', 'latitude', 'longitude', 'plus_code', 'status']);
        foreach ($individuals as $k => $d) {
            $d['distance'] = round($this->getDistanceBetweenPoints($d->latitude, $d->longitude, $lat, $lng), 2);
            if ($d['distance'] > $range) {
                $individuals->forget($k);
            }
        }

        return $this->sendResponse($individuals->toArray(), 'Individuals retrieved successfully');
    }
    public function getDistanceBetweenPoints($lat1, $lon1, $lat2, $lon2)
    {
        $theta = $lon1 - $lon2;
        $miles = (sin(deg2rad($lat1)) * sin(deg2rad($lat2))) + (cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta)));
        $miles = acos($miles);
        $miles = rad2deg($miles);
        $miles = $miles * 60 * 1.1515;
        //$feet = $miles * 5280;
        //$yards = $feet / 3;
        $kilometers = $miles * 1.609344;
        //$meters = $kilometers * 1000;
        return $kilometers;
    }
    /**
     * Display a listing of the Individual.
     * GET|HEAD /individuals
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $input = $request->all();
        if ($input['user_type'] == 'doctors') {
            $individuals = Individual::orderBy('name', 'asc')->get();
            return $this->sendResponse($individuals->toArray(), 'Individuals retrieved successfully');
        } elseif ($input['user_type'] == 'officials') {
            $official = User::find($request->client_id);
            $level = $official->level;
            if ($level == 'admin') {
                $individuals = Individual::all();
            } elseif ($level == 'ward') {
                $individuals = Individual::where([[$level, $official->$level], ['municipality', $official->municipality]])->orderBy('status', 'desc')->get();
            } else {
                $individuals = Individual::where($level, $official->$level)->orderBy('status', 'desc')->get();
            }
            return $this->sendResponse($individuals->toArray(), 'Individuals retrieved successfully');
        }
    }
    public function dependent(Request $request)
    {
        $individuals = Individual::where('parent_id', $request->all()['id'])->orderBy('name', 'asc')->get();

        return $this->sendResponse($individuals->toArray(), 'Individuals retrieved successfully');
    }
    public function suspect(Request $request)
    {
        $individuals = Individual::where('status', '>', 3)->orderBy('status', 'desc')->get();

        return $this->sendResponse($individuals->toArray(), 'Suspected Individuals retrieved successfully');
    }

    public function byStatus($status)
    {
        $individuals = Individual::where('status', $status)->orderBy('status', 'desc')->get();

        return $this->sendResponse($individuals->toArray(), 'Individuals with status ' . $status . ' retrieved successfully');
    }

    /**
     * Store a newly created Individual in storage.
     * POST /individuals
     *
     * @param CreateIndividualAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateIndividualAPIRequest $request)
    {
        $input = $request->all();

        if ($request->hasFile('profile_pic')) {
            $file = $request->file('profile_pic');
            $fileNameWithExt = $request->file('profile_pic')->getClientOriginalName();
            $filename_temp = preg_replace('/[^a-zA-Z0-9_-]+/', '', pathinfo($fileNameWithExt, PATHINFO_FILENAME));
            $extension = $request->file('profile_pic')->getClientOriginalExtension();
            $filename = $filename_temp . '_' . time() . '.' . $extension;
            $input['profile_pic'] = 'storage/individuals/' . $filename;
            $path = $file->storeAs('individuals', $filename, 'public');
        }

        $individual = $this->individualRepository->create($input);

        return $this->sendResponse($individual->toArray(), 'Individual saved successfully');
    }

    /**
     * Display the specified Individual.
     * GET|HEAD /individuals/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Individual $individual */
        $individual = $this->individualRepository->find($id);

        if (empty($individual)) {
            return $this->sendError('Individual not found');
        }

        return $this->sendResponse($individual->toArray(), 'Individual retrieved successfully');
    }

    /**
     * Update the specified Individual in storage.
     * PUT/PATCH /individuals/{id}
     *
     * @param int $id
     * @param UpdateIndividualAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateIndividualAPIRequest $request)
    {
        $input = $request->all();
        /** @var Individual $individual */
        $individual = $this->individualRepository->find($id);

        if (empty($individual)) {
            return $this->sendError('Individual not found');
        }
        if ($request->hasFile('profile_pic')) {
            $file = $request->file('profile_pic');
            $fileNameWithExt = $request->file('profile_pic')->getClientOriginalName();
            $filename_temp = preg_replace('/[^a-zA-Z0-9_-]+/', '', pathinfo($fileNameWithExt, PATHINFO_FILENAME));
            $extension = $request->file('profile_pic')->getClientOriginalExtension();
            $filename = $filename_temp . '_' . time() . '.' . $extension;
            $input['profile_pic'] = 'storage/individuals/' . $filename;
            $path = $file->storeAs('individuals', $filename, 'public');
        }

        $individual = $this->individualRepository->update($input, $id);

        return $this->sendResponse($individual->toArray(), 'Individual updated successfully');
    }

    /**
     * Remove the specified Individual from storage.
     * DELETE /individuals/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Individual $individual */
        $individual = $this->individualRepository->find($id);

        if (empty($individual)) {
            return $this->sendError('Individual not found');
        }

        $individual->delete();

        return $this->sendSuccess('Individual deleted successfully');
    }


    public function searchByEmail(Request $request)
    {
        $input = $request->all();
        $individual = Individual::where('email', $input['email'])->first();
        if ($individual) {
            return $this->sendResponse($individual->toArray(), 'Individual retrieved successfully');
        } else {
            return $this->sendError('email not found');
        }
    }
    public function searchMacAddress(Request $request)
    {
        $input = $request->all();
        $friend_mac_address = $input['friend_mac_address'];
        $meeting_end_time = $input['meeting_end_time'];
        $start_dateTime = new DateTime($meeting_end_time);
        $end_dateTime = new DateTime($meeting_end_time);
        $start = $start_dateTime->modify('-5 minutes');
        $end = $end_dateTime->modify('+5 minutes');
        $friend  = Individual::where('mac_address', $friend_mac_address)->first();
        $meeting = Meeting::whereBetween('date', [$start, $end])->where('individual_id', $friend->id)->get();
        // dd($meeting->count());
        if ($meeting->count() == 1) {
            return $this->sendSuccess(['mac_address' => $meeting[0]->friend_mac_address], 'Individual deleted successfully');
        } else {
            return $this->sendError('mac_address not found');
        }
    }
}
