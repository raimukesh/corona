<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateDangerZoneAPIRequest;
use App\Http\Requests\API\UpdateDangerZoneAPIRequest;
use App\Models\DangerZone;
use App\Repositories\DangerZoneRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class DangerZoneController
 * @package App\Http\Controllers\API
 */

class DangerZoneAPIController extends AppBaseController
{
    /** @var  DangerZoneRepository */
    private $dangerZoneRepository;

    public function __construct(DangerZoneRepository $dangerZoneRepo)
    {
        $this->dangerZoneRepository = $dangerZoneRepo;
    }

    /**
     * Display a listing of the DangerZone.
     * GET|HEAD /dangerZones
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $dangerZones = $this->dangerZoneRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($dangerZones->toArray(), 'Danger Zones retrieved successfully');
    }

    /**
     * Store a newly created DangerZone in storage.
     * POST /dangerZones
     *
     * @param CreateDangerZoneAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateDangerZoneAPIRequest $request)
    {
        $input = $request->all();

        $dangerZone = $this->dangerZoneRepository->create($input);

        return $this->sendResponse($dangerZone->toArray(), 'Danger Zone saved successfully');
    }

    /**
     * Display the specified DangerZone.
     * GET|HEAD /dangerZones/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var DangerZone $dangerZone */
        $dangerZone = $this->dangerZoneRepository->find($id);

        if (empty($dangerZone)) {
            return $this->sendError('Danger Zone not found');
        }

        return $this->sendResponse($dangerZone->toArray(), 'Danger Zone retrieved successfully');
    }

    /**
     * Update the specified DangerZone in storage.
     * PUT/PATCH /dangerZones/{id}
     *
     * @param int $id
     * @param UpdateDangerZoneAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDangerZoneAPIRequest $request)
    {
        $input = $request->all();

        /** @var DangerZone $dangerZone */
        $dangerZone = $this->dangerZoneRepository->find($id);

        if (empty($dangerZone)) {
            return $this->sendError('Danger Zone not found');
        }

        $dangerZone = $this->dangerZoneRepository->update($input, $id);

        return $this->sendResponse($dangerZone->toArray(), 'DangerZone updated successfully');
    }

    /**
     * Remove the specified DangerZone from storage.
     * DELETE /dangerZones/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var DangerZone $dangerZone */
        $dangerZone = $this->dangerZoneRepository->find($id);

        if (empty($dangerZone)) {
            return $this->sendError('Danger Zone not found');
        }

        $dangerZone->delete();

        return $this->sendSuccess('Danger Zone deleted successfully');
    }
}
