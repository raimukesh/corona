<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateNepalTrendAPIRequest;
use App\Http\Requests\API\UpdateNepalTrendAPIRequest;
use App\Models\NepalTrend;
use App\Repositories\NepalTrendRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Carbon\Carbon;
use DB;
use Response;

/**
 * Class NepalTrendController
 * @package App\Http\Controllers\API
 */

class NepalTrendAPIController extends AppBaseController
{
    /** @var  NepalTrendRepository */
    private $nepalTrendRepository;

    public function __construct(NepalTrendRepository $nepalTrendRepo)
    {
        $this->nepalTrendRepository = $nepalTrendRepo;
    }

    /**
     * Display a listing of the NepalTrend.
     * GET|HEAD /nepalTrends
     *
     * @param Request $request
     * @return Response
     */
    public function today(Request $request)
    {
        $nepalTrend = NepalTrend::orderBy('id', 'desc')->first();
        if (Carbon::today()->toDateString() != $nepalTrend->created_at->toDateString()) {
            return $this->sendError('Data is not avaiable');
        } else {
            $nepalTrends = NepalTrend::select(DB::raw('max(id) maxid, date(created_at) as d'))
                ->groupBy('d')
                ->whereBetween('created_at', [Carbon::yesterday()->toDateString(), Carbon::now()])
                ->orderBy('maxid', 'desc')
                ->get();
            if ($nepalTrends->count() != 2) {
                return $this->sendError('Data is not avaiable');
            }
            foreach ($nepalTrends as $nt) {
                $twoDaysTrends[] = NepalTrend::find($nt);
            }
            $diff = [
                'tested_positive' => $twoDaysTrends[0][0]->tested_positive - $twoDaysTrends[1][0]->tested_positive,
                'tested_negative' => $twoDaysTrends[0][0]->tested_negative - $twoDaysTrends[1][0]->tested_negative,
                'tested_total' => $twoDaysTrends[0][0]->tested_total - $twoDaysTrends[1][0]->tested_total,
                'in_isolation' => $twoDaysTrends[0][0]->in_isolation - $twoDaysTrends[1][0]->in_isolation,
                'recovered' => $twoDaysTrends[0][0]->recovered - $twoDaysTrends[1][0]->recovered,
                'deaths' => $twoDaysTrends[0][0]->deaths - $twoDaysTrends[1][0]->deaths,
                'rdt' => $twoDaysTrends[0][0]->rdt - $twoDaysTrends[1][0]->rdt,
                'quarantine' => $twoDaysTrends[0][0]->quarantine - $twoDaysTrends[1][0]->quarantine
            ];
        }

        return $this->sendResponse($diff, 'Nepal Trends retrieved successfully');
    }
    public function index(Request $request)
    {
        $nepalTrends = NepalTrend::orderBy('id', 'desc')->first();

        return $this->sendResponse($nepalTrends->toArray(), 'Nepal Todays Trend retrieved successfully');
    }

    /**
     * Store a newly created NepalTrend in storage.
     * POST /nepalTrends
     *
     * @param CreateNepalTrendAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateNepalTrendAPIRequest $request)
    {
        $input = $request->all();

        $nepalTrend = $this->nepalTrendRepository->create($input);

        return $this->sendResponse($nepalTrend->toArray(), 'Nepal Trend saved successfully');
    }

    /**
     * Display the specified NepalTrend.
     * GET|HEAD /nepalTrends/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var NepalTrend $nepalTrend */
        $nepalTrend = $this->nepalTrendRepository->find($id);

        if (empty($nepalTrend)) {
            return $this->sendError('Nepal Trend not found');
        }

        return $this->sendResponse($nepalTrend->toArray(), 'Nepal Trend retrieved successfully');
    }

    /**
     * Update the specified NepalTrend in storage.
     * PUT/PATCH /nepalTrends/{id}
     *
     * @param int $id
     * @param UpdateNepalTrendAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateNepalTrendAPIRequest $request)
    {
        $input = $request->all();

        /** @var NepalTrend $nepalTrend */
        $nepalTrend = $this->nepalTrendRepository->find($id);

        if (empty($nepalTrend)) {
            return $this->sendError('Nepal Trend not found');
        }

        $nepalTrend = $this->nepalTrendRepository->update($input, $id);

        return $this->sendResponse($nepalTrend->toArray(), 'NepalTrend updated successfully');
    }

    /**
     * Remove the specified NepalTrend from storage.
     * DELETE /nepalTrends/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var NepalTrend $nepalTrend */
        $nepalTrend = $this->nepalTrendRepository->find($id);

        if (empty($nepalTrend)) {
            return $this->sendError('Nepal Trend not found');
        }

        $nepalTrend->delete();

        return $this->sendSuccess('Nepal Trend deleted successfully');
    }
}
