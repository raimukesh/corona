<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateMisscallAPIRequest;
use App\Http\Requests\API\UpdateMisscallAPIRequest;
use App\Models\Misscall;
use App\Repositories\MisscallRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class MisscallController
 * @package App\Http\Controllers\API
 */

class MisscallAPIController extends AppBaseController
{
    /** @var  MisscallRepository */
    private $misscallRepository;

    public function __construct(MisscallRepository $misscallRepo)
    {
        $this->misscallRepository = $misscallRepo;
    }

    /**
     * Display a listing of the Misscall.
     * GET|HEAD /misscalls
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $misscalls = $this->misscallRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($misscalls->toArray(), 'Misscalls retrieved successfully');
    }

    /**
     * Store a newly created Misscall in storage.
     * POST /misscalls
     *
     * @param CreateMisscallAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateMisscallAPIRequest $request)
    {
        $input = $request->all();

        $misscall = $this->misscallRepository->create($input);

        return $this->sendResponse($misscall->toArray(), 'Misscall saved successfully');
    }

    /**
     * Display the specified Misscall.
     * GET|HEAD /misscalls/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Misscall $misscall */
        $misscall = $this->misscallRepository->find($id);

        if (empty($misscall)) {
            return $this->sendError('Misscall not found');
        }

        return $this->sendResponse($misscall->toArray(), 'Misscall retrieved successfully');
    }

    /**
     * Update the specified Misscall in storage.
     * PUT/PATCH /misscalls/{id}
     *
     * @param int $id
     * @param UpdateMisscallAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMisscallAPIRequest $request)
    {
        $input = $request->all();

        /** @var Misscall $misscall */
        $misscall = $this->misscallRepository->find($id);

        if (empty($misscall)) {
            return $this->sendError('Misscall not found');
        }

        $misscall = $this->misscallRepository->update($input, $id);

        return $this->sendResponse($misscall->toArray(), 'Misscall updated successfully');
    }

    /**
     * Remove the specified Misscall from storage.
     * DELETE /misscalls/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Misscall $misscall */
        $misscall = $this->misscallRepository->find($id);

        if (empty($misscall)) {
            return $this->sendError('Misscall not found');
        }

        $misscall->delete();

        return $this->sendSuccess('Misscall deleted successfully');
    }
}
