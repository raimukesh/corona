<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSafetyAPIRequest;
use App\Http\Requests\API\UpdateSafetyAPIRequest;
use App\Models\Safety;
use App\Repositories\SafetyRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class SafetyController
 * @package App\Http\Controllers\API
 */

class SafetyAPIController extends AppBaseController
{
    /** @var  SafetyRepository */
    private $safetyRepository;

    public function __construct(SafetyRepository $safetyRepo)
    {
        $this->safetyRepository = $safetyRepo;
    }

    /**
     * Display a listing of the Safety.
     * GET|HEAD /safeties
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $safeties = Safety::orderBy('id', 'desc')->get();
        return $this->sendResponse($safeties->toArray(), 'Safeties retrieved successfully');
    }

    /**
     * Store a newly created Safety in storage.
     * POST /safeties
     *
     * @param CreateSafetyAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateSafetyAPIRequest $request)
    {
        $input = $request->all();

        $safety = $this->safetyRepository->create($input);

        return $this->sendResponse($safety->toArray(), 'Safety saved successfully');
    }

    /**
     * Display the specified Safety.
     * GET|HEAD /safeties/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Safety $safety */
        $safety = $this->safetyRepository->find($id);

        if (empty($safety)) {
            return $this->sendError('Safety not found');
        }

        return $this->sendResponse($safety->toArray(), 'Safety retrieved successfully');
    }

    /**
     * Update the specified Safety in storage.
     * PUT/PATCH /safeties/{id}
     *
     * @param int $id
     * @param UpdateSafetyAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSafetyAPIRequest $request)
    {
        $input = $request->all();

        /** @var Safety $safety */
        $safety = $this->safetyRepository->find($id);

        if (empty($safety)) {
            return $this->sendError('Safety not found');
        }

        $safety = $this->safetyRepository->update($input, $id);

        return $this->sendResponse($safety->toArray(), 'Safety updated successfully');
    }

    /**
     * Remove the specified Safety from storage.
     * DELETE /safeties/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Safety $safety */
        $safety = $this->safetyRepository->find($id);

        if (empty($safety)) {
            return $this->sendError('Safety not found');
        }

        $safety->delete();

        return $this->sendSuccess('Safety deleted successfully');
    }
}
