<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Doctor;
use App\Models\Individual;
use App\Models\Official;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\VerifiesEmails;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Verified;

class VerificationApiController extends Controller
{
    use VerifiesEmails;
    /**
     * Show the email verification notice.
     *
     */
    public function show()
    {
        //
    }
    /**
     * Mark the authenticated user's email address as verified.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function verify(Request $request)
    {
        // dd(Carbon::createFromTimestamp($request->all()['expires']));
        if (Carbon::createFromTimestamp($request->all()['expires'])->lt(Carbon::now())) {
            return response()->json('Session has been exipred you need to verifiy your account within 1 hour!');
        }
        $userID = $request['id'];
        $user = '';
        if (\Request::is('*doctor*')) {
            $user = Doctor::findOrFail($userID);
        } elseif (\Request::is('*individual*')) {
            $user = Individual::findOrFail($userID);
        } elseif (\Request::is('*official*')) {
            $user = User::findOrFail($userID);
        }
        $date = date('Y-m-d g:i:s');
        $user->email_verified_at = $date; // to enable the 'email_verified_at field of that user be a current time stamp by mimicing the must verify email feature
        $user->save();
        return response()->json('Email verified!');
    }
    /**
     * Resend the email verification notification.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function resend(Request $request)
    {
        if ($request->user()->hasVerifiedEmail()) {
            return response()->json('User already have verified email!', 422);
            // return redirect($this->redirectPath());
        }
        $request->user()->sendEmailVerificationNotification();
        return response()->json('The notification has been resubmitted');
        // return back()->with('resent', true);
    }
}
