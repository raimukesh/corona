<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateDangerZoneRequest;
use App\Http\Requests\UpdateDangerZoneRequest;
use App\Repositories\DangerZoneRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class DangerZoneController extends AppBaseController
{
    /** @var  DangerZoneRepository */
    private $dangerZoneRepository;

    public function __construct(DangerZoneRepository $dangerZoneRepo)
    {
        $this->dangerZoneRepository = $dangerZoneRepo;
    }

    /**
     * Display a listing of the DangerZone.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $dangerZones = $this->dangerZoneRepository->paginate(10);

        return view('danger_zones.index')
            ->with('dangerZones', $dangerZones);
    }

    /**
     * Show the form for creating a new DangerZone.
     *
     * @return Response
     */
    public function create()
    {
        return view('danger_zones.create');
    }

    /**
     * Store a newly created DangerZone in storage.
     *
     * @param CreateDangerZoneRequest $request
     *
     * @return Response
     */
    public function store(CreateDangerZoneRequest $request)
    {
        $input = $request->all();

        $dangerZone = $this->dangerZoneRepository->create($input);

        Flash::success('Danger Zone saved successfully.');

        return redirect(route('dangerZones.index'));
    }

    /**
     * Display the specified DangerZone.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $dangerZone = $this->dangerZoneRepository->find($id);

        if (empty($dangerZone)) {
            Flash::error('Danger Zone not found');

            return redirect(route('dangerZones.index'));
        }

        return view('danger_zones.show')->with('dangerZone', $dangerZone);
    }

    /**
     * Show the form for editing the specified DangerZone.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $dangerZone = $this->dangerZoneRepository->find($id);

        if (empty($dangerZone)) {
            Flash::error('Danger Zone not found');

            return redirect(route('dangerZones.index'));
        }

        return view('danger_zones.edit')->with('dangerZone', $dangerZone);
    }

    /**
     * Update the specified DangerZone in storage.
     *
     * @param int $id
     * @param UpdateDangerZoneRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDangerZoneRequest $request)
    {
        $dangerZone = $this->dangerZoneRepository->find($id);

        if (empty($dangerZone)) {
            Flash::error('Danger Zone not found');

            return redirect(route('dangerZones.index'));
        }

        $dangerZone = $this->dangerZoneRepository->update($request->all(), $id);

        Flash::success('Danger Zone updated successfully.');

        return redirect(route('dangerZones.index'));
    }

    /**
     * Remove the specified DangerZone from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $dangerZone = $this->dangerZoneRepository->find($id);

        if (empty($dangerZone)) {
            Flash::error('Danger Zone not found');

            return redirect(route('dangerZones.index'));
        }

        $this->dangerZoneRepository->delete($id);

        Flash::success('Danger Zone deleted successfully.');

        return redirect(route('dangerZones.index'));
    }
}
