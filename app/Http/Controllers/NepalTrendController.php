<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateNepalTrendRequest;
use App\Http\Requests\UpdateNepalTrendRequest;
use App\Repositories\NepalTrendRepository;
use App\Http\Controllers\AppBaseController;
use App\Models\NepalTrend;
use Illuminate\Http\Request;
use Flash;
use Response;

class NepalTrendController extends AppBaseController
{
    /** @var  NepalTrendRepository */
    private $nepalTrendRepository;

    public function __construct(NepalTrendRepository $nepalTrendRepo)
    {
        $this->nepalTrendRepository = $nepalTrendRepo;
    }

    /**
     * Display a listing of the NepalTrend.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $nepalTrends = NepalTrend::orderBy('id', 'desc')->paginate(10);

        return view('nepal_trends.index')
            ->with('nepalTrends', $nepalTrends);
    }

    /**
     * Show the form for creating a new NepalTrend.
     *
     * @return Response
     */
    public function create()
    {
        return view('nepal_trends.create');
    }

    /**
     * Store a newly created NepalTrend in storage.
     *
     * @param CreateNepalTrendRequest $request
     *
     * @return Response
     */
    public function store(CreateNepalTrendRequest $request)
    {
        $input = $request->all();

        $nepalTrend = $this->nepalTrendRepository->create($input);

        Flash::success('Nepal Trend saved successfully.');

        return redirect(route('nepalTrends.index'));
    }

    /**
     * Display the specified NepalTrend.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $nepalTrend = $this->nepalTrendRepository->find($id);

        if (empty($nepalTrend)) {
            Flash::error('Nepal Trend not found');

            return redirect(route('nepalTrends.index'));
        }

        return view('nepal_trends.show')->with('nepalTrend', $nepalTrend);
    }

    /**
     * Show the form for editing the specified NepalTrend.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $nepalTrend = $this->nepalTrendRepository->find($id);

        if (empty($nepalTrend)) {
            Flash::error('Nepal Trend not found');

            return redirect(route('nepalTrends.index'));
        }

        return view('nepal_trends.edit')->with('nepalTrend', $nepalTrend);
    }

    /**
     * Update the specified NepalTrend in storage.
     *
     * @param int $id
     * @param UpdateNepalTrendRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateNepalTrendRequest $request)
    {
        $nepalTrend = $this->nepalTrendRepository->find($id);

        if (empty($nepalTrend)) {
            Flash::error('Nepal Trend not found');

            return redirect(route('nepalTrends.index'));
        }

        $nepalTrend = $this->nepalTrendRepository->update($request->all(), $id);

        Flash::success('Nepal Trend updated successfully.');

        return redirect(route('nepalTrends.index'));
    }

    /**
     * Remove the specified NepalTrend from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $nepalTrend = $this->nepalTrendRepository->find($id);

        if (empty($nepalTrend)) {
            Flash::error('Nepal Trend not found');

            return redirect(route('nepalTrends.index'));
        }

        $this->nepalTrendRepository->delete($id);

        Flash::success('Nepal Trend deleted successfully.');

        return redirect(route('nepalTrends.index'));
    }
}
