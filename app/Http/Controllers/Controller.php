<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use InfyOm\Generator\Utils\ResponseUtil;
use Response;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $dr_post = [
        0 => "Dr. Doctor",
        1 => "P.H.O Public Health Officer",
        2 => "N.O Nursing Officer",
        3 => "L.O. Lab Officer",
        4 => "H.A. Health Assistant",
        5 => "St. Nr. Staff Nurse",
        6 => "L.T. Lab Technician",
        7 => "A.H.W. Auxillary Health Worker",
        8 => "A.N.M. Auxillary Nurse Mudwifery",
        9 => "L.A. Lab Assistants"
    ];

    public function sendResponse($result, $message)
    {
        return Response::json(ResponseUtil::makeResponse($message, $result));
    }

    public function sendError($error, $code = 404)
    {
        return Response::json(ResponseUtil::makeError($error), $code);
    }

    public function sendSuccess($message)
    {
        return Response::json([
            'success' => true,
            'message' => $message
        ], 200);
    }
}
