<?php

namespace App\Http\Controllers;

use App\Models\Individual;
use App\Models\NepalTrend;
use Carbon\Carbon;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $nepalTrend = NepalTrend::orderBy('id', 'desc')->first();
        return view('home', compact('nepalTrend'));
    }
    public function login()
    {
        return view('home');
    }

    public function deviceLocation()
    {
        $individuals = Individual::all();
        return view('device_location', compact('individuals'));
    }
}
