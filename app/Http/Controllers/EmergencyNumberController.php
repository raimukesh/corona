<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateEmergencyNumberRequest;
use App\Http\Requests\UpdateEmergencyNumberRequest;
use App\Repositories\EmergencyNumberRepository;
use App\Http\Controllers\AppBaseController;
use App\Models\EmergencyNumber;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Flash;
use Response;

class EmergencyNumberController extends AppBaseController
{
    /** @var  EmergencyNumberRepository */
    private $emergencyNumberRepository;

    public function __construct(EmergencyNumberRepository $emergencyNumberRepo)
    {
        $this->emergencyNumberRepository = $emergencyNumberRepo;
    }

    /**
     * Display a listing of the EmergencyNumber.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $emergencyNumbers = EmergencyNumber::orderBy('id', 'desc')->get();

        return view('emergency_numbers.index')
            ->with('emergencyNumbers', $emergencyNumbers);
    }

    /**
     * Show the form for creating a new EmergencyNumber.
     *
     * @return Response
     */
    public function create()
    {
        return view('emergency_numbers.create');
    }

    /**
     * Store a newly created EmergencyNumber in storage.
     *
     * @param CreateEmergencyNumberRequest $request
     *
     * @return Response
     */
    public function store(CreateEmergencyNumberRequest $request)
    {
        $input = $request->all();

        $emergencyNumber = $this->emergencyNumberRepository->create($input);

        Flash::success('Emergency Number saved successfully.');

        return redirect(route('emergencyNumbers.index'));
    }

    /**
     * Display the specified EmergencyNumber.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $emergencyNumber = $this->emergencyNumberRepository->find($id);

        if (empty($emergencyNumber)) {
            Flash::error('Emergency Number not found');

            return redirect(route('emergencyNumbers.index'));
        }

        return view('emergency_numbers.show')->with('emergencyNumber', $emergencyNumber);
    }

    /**
     * Show the form for editing the specified EmergencyNumber.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $emergencyNumber = $this->emergencyNumberRepository->find($id);

        if (empty($emergencyNumber)) {
            Flash::error('Emergency Number not found');

            return redirect(route('emergencyNumbers.index'));
        }

        return view('emergency_numbers.edit')->with('emergencyNumber', $emergencyNumber);
    }

    /**
     * Update the specified EmergencyNumber in storage.
     *
     * @param int $id
     * @param UpdateEmergencyNumberRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateEmergencyNumberRequest $request)
    {
        $emergencyNumber = $this->emergencyNumberRepository->find($id);

        if (empty($emergencyNumber)) {
            Flash::error('Emergency Number not found');

            return redirect(route('emergencyNumbers.index'));
        }

        $emergencyNumber = $this->emergencyNumberRepository->update($request->all(), $id);

        Flash::success('Emergency Number updated successfully.');

        return redirect(route('emergencyNumbers.index'));
    }

    /**
     * Remove the specified EmergencyNumber from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $emergencyNumber = $this->emergencyNumberRepository->find($id);

        if (empty($emergencyNumber)) {
            Flash::error('Emergency Number not found');

            return redirect(route('emergencyNumbers.index'));
        }

        $this->emergencyNumberRepository->delete($id);

        Flash::success('Emergency Number deleted successfully.');

        return redirect(route('emergencyNumbers.index'));
    }

    public function checkGuard()
    {
        if (Auth::guard()->check()) {
            $path = "";
            return $path;
        } elseif (Auth::guard('official')->check()) {
            $path = "auth_official.pages.";
            return $path;
        }
    }
}
