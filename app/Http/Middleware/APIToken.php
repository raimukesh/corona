<?php

namespace App\Http\Middleware;

use App\Models\Client;
use App\Models\Doctor;
use App\Models\Individual;
use App\Models\Official;
use App\User;
use Closure;
use InfyOm\Generator\Utils\ResponseUtil;
use Response;

class APIToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->header('Authorization')) {
            $token = $request->header('Authorization');
            $client = '';
            if ($request->all()['user_type'] == 'doctors') {
                $client = Doctor::where('api_token', $request->bearerToken())->first();
            } elseif ($request->all()['user_type'] == 'individuals') {
                $client = Individual::where('api_token', $request->bearerToken())->first();
            } elseif ($request->all()['user_type'] == 'officials') {
                $client = User::where('api_token', $request->bearerToken())->first();
            } else {
                return $this->sendError('Invalid user_type');
            }
            if ($client) {
                $request->merge(['client_id' => $client->id]);
                return $next($request);
            } else {
                return $this->sendError('Token Mismatch');
            }
        }
        return $this->sendError('Not a valid API request');
    }

    public function sendError($error, $code = 404)
    {
        return Response::json(ResponseUtil::makeError($error), $code);
    }
}
