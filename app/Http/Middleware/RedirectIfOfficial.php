<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfOfficial
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handle($request, Closure $next, $guard = "official")
    {
        // dd(Auth::guard($guard));
        if (!Auth::guard($guard)->check()) {
            return redirect(route('official.login'));
        }

        return $next($request);
    }
}
