<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class EmergencyNumber
 * @package App\Models
 * @version March 29, 2020, 10:54 am +0545
 *
 * @property string title
 * @property string number
 */
class EmergencyNumber extends Model
{

    public $table = 'emergency_numbers';
    



    public $fillable = [
        'title',
        'number'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'number' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
