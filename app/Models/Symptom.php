<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Symptom
 * @package App\Models
 * @version March 24, 2020, 10:03 am +0545
 *
 * @property string name
 */
class Symptom extends Model
{

    public $table = 'symptoms';
    public $timestamps = false;


    public $fillable = [
        'name', 'name_in_nepali', 'color', 'icon'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'name_in_nepali' => 'string',
        'color' => 'string',
        'icon' => 'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [];
}
