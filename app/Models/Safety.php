<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Safety
 * @package App\Models
 * @version March 31, 2020, 10:59 am +0545
 *
 * @property string title
 * @property string description
 */
class Safety extends Model
{

    public $table = 'safeties';
    



    public $fillable = [
        'title',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
