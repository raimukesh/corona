<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class History
 * @package App\Models
 * @version March 24, 2020, 10:47 am +0545
 *
 * @property \App\Models\Individual individual
 * @property string symptoms
 * @property integer individual_id
 * @property string review
 * @property integer admin_comment
 */
class History extends Model
{

    public $table = 'histories';
    



    public $fillable = [
        'symptoms',
        'individual_id',
        'review',
        'importance',
        'approved_review_count',

    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'individual_id' => 'integer',
        'approved_review_count' => 'integer',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function individual()
    {
        return $this->belongsTo(\App\Models\Individual::class, 'individual_id', 'id');
    }
}
