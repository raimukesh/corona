<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Announcement
 * @package App\Models
 * @version March 31, 2020, 10:54 am +0545
 *
 * @property string title
 * @property string description
 */
class Announcement extends Model
{

    public $table = 'announcements';




    public $fillable = [
        'title',
        'thumbnail',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'thumbnail' => 'string',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [];
}
