<?php

namespace App\Models;

use Eloquent as Model;
use App\Notifications\VerifyApiEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Illuminate\Notifications\Notifiable;

/**
 * Class Official
 * @package App\Models
 * @version March 23, 2020, 1:20 pm +0545
 *
 * @property string name
 * @property string phone
 * @property integer ward
 * @property string municipality
 * @property integer province
 * @property string address
 * @property string email
 * @property string password
 * @property string email_verified_at
 * @property string api_token
 */
class Official extends Authenticatable
{
    use Notifiable;

    public $fillable = [
        'name',
        'phone',
        'ward',
        'municipality',
        'district',
        'province',
        'address',
        'email',
        'password',
        'email_verified_at',
        'api_token'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'phone' => 'string',
        'ward' => 'integer',
        'municipality' => 'string',
        'district' => 'string',
        'province' => 'string',
        'address' => 'string',
        'email' => 'string',
        'password' => 'string',
        'api_token' => 'string'
    ];

    protected $hidden = [
        'password'
    ];
    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [];
    public function sendApiEmailVerificationNotification()
    {
        $this->notify(new VerifyApiEmail('official')); // my notification
    }
}
