<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class DangerZone
 * @package App\Models
 * @version May 21, 2020, 12:02 pm +0545
 *
 * @property string mac_address
 * @property number latitude
 * @property number longitude
 * @property string address
 */
class DangerZone extends Model
{

    public $table = 'danger_zones';
    



    public $fillable = [
        'mac_address',
        'latitude',
        'longitude',
        'address'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'mac_address' => 'string',
        'latitude' => 'double',
        'longitude' => 'double',
        'address' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
