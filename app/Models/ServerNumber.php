<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class ServerNumber
 * @package App\Models
 * @version April 20, 2020, 1:13 pm +0545
 *
 * @property string number
 */
class ServerNumber extends Model
{

    public $table = 'server_numbers';
    



    public $fillable = [
        'number'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'number' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
