<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Location
 * @package App\Models
 * @version March 29, 2020, 1:11 pm +0545
 *
 * @property string local_level_en
 * @property string local_level_np
 * @property string district
 * @property string province
 */
class Location extends Model
{

    public $table = 'locations';
    



    public $fillable = [
        'local_level_en',
        'local_level_np',
        'district',
        'province'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'local_level_en' => 'string',
        'local_level_np' => 'string',
        'district' => 'string',
        'province' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
