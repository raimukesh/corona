<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Meeting
 * @package App\Models
 * @version April 17, 2020, 10:10 am +0545
 *
 * @property \App\Models\User individual
 * @property \App\Models\User friend
 * @property integer individual_id
 * @property integer rssi
 * @property integer duration
 * @property integer distance
 * @property integer friend_id
 * @property number latitude
 * @property number longitude
 * @property string date
 */
class Meeting extends Model
{

    public $table = 'meetings';




    public $fillable = [
        'individual_id',
        'rssi',
        'duration',
        'distance',
        'friend_mac_address',
        'latitude',
        'longitude',
        'date'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'individual_id' => 'integer',
        'rssi' => 'integer',
        'duration' => 'integer',
        'distance' => 'integer',
        'friend_mac_address' => 'string',
        'latitude' => 'double',
        'longitude' => 'double',
        'date' => 'datetime',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function individual()
    {
        return $this->belongsTo(\App\Models\Individual::class, 'individual_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
}
