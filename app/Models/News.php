<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class News
 * @package App\Models
 * @version March 29, 2020, 10:52 am +0545
 *
 * @property string title
 * @property string description
 */
class News extends Model
{

    public $table = 'news';




    public $fillable = [
        'title',
        'thumbnail',
        'description',
        'source'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'thumbnail' => 'string',
        'description' => 'string',
        'source' => 'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [];
}
