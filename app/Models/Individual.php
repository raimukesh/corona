<?php

namespace App\Models;

use Eloquent as Model;
use App\Notifications\VerifyApiEmail;
use Illuminate\Notifications\Notifiable;


/**
 * Class Individual
 * @package App\Models
 * @version March 23, 2020, 9:52 am +0545
 *
 * @property string name
 * @property string phone
 * @property integer ward
 * @property string municipality
 * @property integer province
 * @property string address
 * @property string email
 * @property string password
 * @property string email_verified_at
 * @property string api_token
 */
class Individual extends Model
{
    use Notifiable;
    public $fillable = [
        'name',
        'phone',
        'ward',
        'municipality',
        'district',
        'province',
        'address',
        'latitude',
        'longitude',
        'current_latitude',
        'current_longitude',
        'plus_code',
        'email',
        'password',
        'email_verified_at',
        'api_token',
        'age',
        'gender',
        'status',
        'parent_id',
        'mac_address',
        'profile_pic',
        'district'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'phone' => 'string',
        'ward' => 'integer',
        'municipality' => 'string',
        'district' => 'string',
        'province' => 'string',
        'address' => 'string',
        'latitude' => 'double',
        'logitude' => 'double',
        'current_latitude' => 'double',
        'current_longitude' => 'double',
        'plus_code' => 'string',
        'email' => 'string',
        'password' => 'string',
        'mac_address' => 'string',
        'parent_id' => 'integer',
        'api_token' => 'string',
        'profile_pic' => 'string',
        'age' => 'integer',

    ];

    protected $hidden = [
        'password',
        'email_verified_at'
    ];
    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [];
    public function sendApiEmailVerificationNotification()
    {
        $this->notify(new VerifyApiEmail('individual')); // my notification
    }
    public function histories()
    {
        return $this->hasMany('App\Models\History');
    }
}
