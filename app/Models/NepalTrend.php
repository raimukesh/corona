<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class NepalTrend
 * @package App\Models
 * @version April 5, 2020, 12:51 pm +0545
 *
 * @property integer tested_positive
 * @property integer tested_negative
 * @property integer tested_total
 * @property integer in_isolation
 * @property integer deaths
 */
class NepalTrend extends Model
{

    public $table = 'nepal_trends';




    public $fillable = [
        'tested_positive',
        'tested_negative',
        'tested_total',
        'in_isolation',
        'deaths',
        'rdt',
        'quarantine',
        'recovered'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'tested_positive' => 'integer',
        'tested_negative' => 'integer',
        'tested_total' => 'integer',
        'in_isolation' => 'integer',
        'recovered' => 'integer',
        'quarantine' => 'integer',
        'rdt' => 'integer',
        'recured' => 'integer',
        'critical' => 'integer',
        'deaths' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [];
}
