<?php

namespace App\Repositories;

use App\Models\NepalTrend;
use App\Repositories\BaseRepository;

/**
 * Class NepalTrendRepository
 * @package App\Repositories
 * @version April 5, 2020, 12:51 pm +0545
*/

class NepalTrendRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return NepalTrend::class;
    }
}
