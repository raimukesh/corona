<?php

namespace App\Repositories;

use App\Models\Safety;
use App\Repositories\BaseRepository;

/**
 * Class SafetyRepository
 * @package App\Repositories
 * @version March 31, 2020, 10:59 am +0545
*/

class SafetyRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Safety::class;
    }
}
