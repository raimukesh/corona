<?php

namespace App\Repositories;

use App\Models\ServerNumber;
use App\Repositories\BaseRepository;

/**
 * Class ServerNumberRepository
 * @package App\Repositories
 * @version April 20, 2020, 1:13 pm +0545
*/

class ServerNumberRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ServerNumber::class;
    }
}
