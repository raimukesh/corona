<?php

namespace App\Repositories;

use App\Models\Symptom;
use App\Repositories\BaseRepository;

/**
 * Class SymptomRepository
 * @package App\Repositories
 * @version March 24, 2020, 10:03 am +0545
*/

class SymptomRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Symptom::class;
    }
}
