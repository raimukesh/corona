<?php

namespace App\Repositories;

use App\Models\Meeting;
use App\Repositories\BaseRepository;

/**
 * Class MeetingRepository
 * @package App\Repositories
 * @version April 17, 2020, 10:10 am +0545
*/

class MeetingRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'user_id',
        'rssi',
        'duration',
        'distance',
        'friend_id',
        'latitude',
        'longitude',
        'date'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Meeting::class;
    }
}
