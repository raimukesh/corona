<?php

namespace App\Repositories;

use App\Models\DangerZone;
use App\Repositories\BaseRepository;

/**
 * Class DangerZoneRepository
 * @package App\Repositories
 * @version May 21, 2020, 12:02 pm +0545
*/

class DangerZoneRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return DangerZone::class;
    }
}
