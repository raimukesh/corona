<?php

namespace App\Repositories;

use App\Models\EmergencyNumber;
use App\Repositories\BaseRepository;

/**
 * Class EmergencyNumberRepository
 * @package App\Repositories
 * @version March 29, 2020, 10:54 am +0545
*/

class EmergencyNumberRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return EmergencyNumber::class;
    }
}
