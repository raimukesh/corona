<?php

namespace App\Repositories;

use App\Models\Misscall;
use App\Repositories\BaseRepository;

/**
 * Class MisscallRepository
 * @package App\Repositories
 * @version April 20, 2020, 1:14 pm +0545
*/

class MisscallRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Misscall::class;
    }
}
