@extends('layouts.app')

@section('content')
<div class="container">
    @if($nepalTrend != null)
    <div class="row" style="padding:2px">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table" id="nepalTrends-table">
                    <thead  style="background:#337ab7">
                        <tr>
                            <th>Tested Positive</th>
                    <th>Tested Negative</th>
                    <th>Tested Total</th>
                    <th>In Isolation</th>
                    <th>Recovered</th>
                    <th>Deaths</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{ $nepalTrend->tested_positive }}</td>
                        <td>{{ $nepalTrend->tested_negative }}</td>
                        <td>{{ $nepalTrend->tested_total }}</td>
                        <td>{{ $nepalTrend->in_isolation }}</td>
                        <td>{{ $nepalTrend->recovered }}</td>
                        <td>{{ $nepalTrend->deaths }}</td>
                        <td>{{ $nepalTrend->created_at }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @endif
    <div class="row">
        <a href="{{route('individuals.index')}}" class="col-md-3" style="background:#51515c; border:5px solid white">
                <div class="text-center"> <b style="font-size:24px;color:white">Patients</b> </div>
            <div><img style="padding:50px" class="img-responsive" src="{{asset('images/icons/birami.png')}}" alt=""></div>
            </a>
        <a href="{{route('doctors.index')}}" class="col-md-3" style="background:#51515c; border:5px solid white">
                <div class="text-center"> <b style="font-size:24px;color:white">Doctor</b> </div>
            <div><img style="padding:50px" class="img-responsive" src="{{asset('images/icons/doctor.png')}}" alt=""></div>
        </a>
        <a href="{{route('deviceLocation')}}" class="col-md-3" style="background:#51515c; border:5px solid white">
                <div class="text-center"> <b style="font-size:24px;color:white">Map</b> </div>
            <div><img style="padding:50px" class="img-responsive" src="{{asset('images/icons/map.png')}}" alt=""></div>
        </a>
        <a href="{{route('emergencyNumbers.index')}}" class="col-md-3" style="background:#51515c; border:5px solid white">
                <div class="text-center"> <b style="font-size:24px;color:white">Emergency Number</b> </div>
            <div><img style="padding:50px" class="img-responsive" src="{{asset('images/icons/number.png')}}" alt=""></div>
        </a>
        <a href="{{route('news.index')}}" class="col-md-3" style="background:#51515c; border:5px solid white">
            <div class="text-center"> <b style="font-size:24px;color:white">News</b> </div>
        <div><img style="padding:50px" class="img-responsive" src="{{asset('images/icons/news.png')}}" alt=""></div>
        </a>
        <a href="{{route('announcements.index')}}" class="col-md-3" style="background:#51515c; border:5px solid white">
            <div class="text-center"> <b style="font-size:24px;color:white">Announcements</b> </div>
        <div><img style="padding:50px" class="img-responsive" src="{{asset('images/icons/announcements.png')}}" alt=""></div>
        </a>
        <a href="{{route('safeties.index')}}" class="col-md-3" style="background:#51515c; border:5px solid white">
            <div class="text-center"> <b style="font-size:24px;color:white">Safeties</b> </div>
        <div><img style="padding:50px" class="img-responsive" src="{{asset('images/icons/safety_tips.png')}}" alt=""></div>
        </a>

    </div>
</div>
@endsection
