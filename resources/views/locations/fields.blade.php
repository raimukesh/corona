<!-- Local Level En Field -->
<div class="form-group col-sm-6">
    {!! Form::label('local_level_en', 'Local Level En:') !!}
    {!! Form::text('local_level_en', null, ['class' => 'form-control']) !!}
</div>

<!-- Local Level Np Field -->
<div class="form-group col-sm-6">
    {!! Form::label('local_level_np', 'Local Level Np:') !!}
    {!! Form::text('local_level_np', null, ['class' => 'form-control']) !!}
</div>

<!-- District Field -->
<div class="form-group col-sm-6">
    {!! Form::label('district', 'District:') !!}
    {!! Form::text('district', null, ['class' => 'form-control']) !!}
</div>

<!-- Province Field -->
<div class="form-group col-sm-6">
    {!! Form::label('province', 'Province:') !!}
    {!! Form::text('province', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('locations.index') }}" class="btn btn-default">Cancel</a>
</div>
