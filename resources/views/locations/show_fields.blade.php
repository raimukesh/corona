<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $location->id }}</p>
</div>

<!-- Local Level En Field -->
<div class="form-group">
    {!! Form::label('local_level_en', 'Local Level En:') !!}
    <p>{{ $location->local_level_en }}</p>
</div>

<!-- Local Level Np Field -->
<div class="form-group">
    {!! Form::label('local_level_np', 'Local Level Np:') !!}
    <p>{{ $location->local_level_np }}</p>
</div>

<!-- District Field -->
<div class="form-group">
    {!! Form::label('district', 'District:') !!}
    <p>{{ $location->district }}</p>
</div>

<!-- Province Field -->
<div class="form-group">
    {!! Form::label('province', 'Province:') !!}
    <p>{{ $location->province }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $location->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $location->updated_at }}</p>
</div>

