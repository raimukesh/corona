<div class="table-responsive">
    <table class="table" id="locations-table">
        <thead>
            <tr>
                <th>S.N.</th>
                <th>Local Level En</th>
                <th>Local Level Np</th>
                <th>District</th>
                <th>Province</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($locations as $location)
            <tr>
                <td>{{ $location->id }} </td>
                <td>{{ $location->local_level_en }}</td>
                <td>{{ $location->local_level_np }}</td>
                <td>{{ $location->district }}</td>
                <td>{{ $location->province }}</td>
                <td>
                    {!! Form::open(['route' => ['locations.destroy', $location->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('locations.show', [$location->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('locations.edit', [$location->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
