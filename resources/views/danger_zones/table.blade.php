<div class="table-responsive">
    <table class="table" id="dangerZones-table">
        <thead>
            <tr>
                <th>Mac Address</th>
        <th>Latitude</th>
        <th>Longitude</th>
        <th>Address</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($dangerZones as $dangerZone)
            <tr>
                <td>{{ $dangerZone->mac_address }}</td>
            <td>{{ $dangerZone->latitude }}</td>
            <td>{{ $dangerZone->longitude }}</td>
            <td>{{ $dangerZone->address }}</td>
                <td>
                    {!! Form::open(['route' => ['dangerZones.destroy', $dangerZone->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('dangerZones.show', [$dangerZone->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('dangerZones.edit', [$dangerZone->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
