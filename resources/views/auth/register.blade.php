@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Users
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'register']) !!}
                        <!-- Name Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('name', 'Name:') !!}
                            {!! Form::text('name', null, ['class' => 'form-control','required'=>true]) !!}
                        </div>

                        <!-- Phone Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('phone', 'Phone:') !!}
                            {!! Form::text('phone', null, ['class' => 'form-control','required'=>true]) !!}
                        </div>

                        <!-- Province Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('province', 'Province:') !!}
                            {!! Form::select('province',$province, null, ['class' => 'form-control','required'=>true,'id'=>'province','onchange'=>'getDistrict(this.value)', 'placeholder' => 'Please Select Any Province']) !!}
                        </div>

                        <!-- District Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('district', 'District:') !!}
                            {!! Form::select('district',array(), null, ['class' => 'form-control','required'=>true,'id'=>'district','onchange'=>'getMunicipality(this.value)', 'placeholder' => 'Please Select Any District']) !!}
                        </div>
        
                        <!-- Municipality Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('municipality', 'Municipality:') !!}
                            {!! Form::select('municipality', array(), null, ['class' => 'form-control','required'=>true,'id'=>'municipality', 'placeholder' => 'Please Select Any Municipality']) !!}
                        </div>

                        <!-- Ward Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('ward', 'Ward:') !!}
                            {!! Form::select('ward', array(1=>1,2=>2,3=>3,4=>4,5=>5,6=>6,7=>7,8=>8,9=>9,10=>10,11=>11,12=>12,13=>13,14=>14,15=>15,16=>16,17=>17,18=>18,19=>19,20=>20,21=>21,22=>22,23=>23,24=>24,25=>25,26=>26,27=>27,28=>28,29=>29,30=>30,31=>31,32=>32), null, ['class' => 'form-control','required'=>true]) !!}
                        </div>

                        <!-- Address Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('address', 'Address:') !!}
                            {!! Form::text('address', null, ['class' => 'form-control','required'=>true]) !!}
                        </div>

                        <!-- Email Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('email', 'Email:') !!}
                            {!! Form::email('email', null, ['class' => 'form-control','required'=>true]) !!}
                        </div>

                        <!-- Password Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('password', 'Password:') !!}
                            {!! Form::password('password', ['class' => 'form-control','required'=>true]) !!}
                        </div>
                        <div class="form-group col-sm-6">
                            {!! Form::label('password_confirmation', 'Confirm Password:') !!}
                            {!! Form::password('password_confirmation', ['class' => 'form-control','required'=>true]) !!}
                        </div>

                        
                        <!-- Level Field -->
                        <div class="form-group col-sm-6">
                            {!! Form::label('level', 'Level:') !!}
                            {!! Form::select('level', array('ward'=>'Ward','municipality'=>'Municipality','district'=>'District','province'=>'Province'),null, ['class' => 'form-control','required'=>true]) !!}
                        </div>

                        <!-- Submit Field -->
                        <div class="form-group col-sm-12">
                            {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                            <a href="{{ route('officials.index') }}" class="btn btn-default">Cancel</a>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
<script type="text/javascript">
    function getDistrict(value)
    { 
        select = document.getElementById('district');
        select.options.length = 1;
        const url = {!! '"'.url('api/locations?province='). '"'!!}+value;
        $.get(url,function(data,status){
            for (var i = 0; i < data.data.length; i++) {
                var option = document.createElement("option");
                option.value = data.data[i].district;
                option.text = data.data[i].district;
                console.log(option.text);
                select.appendChild(option);
            }           
        });
    }
    function getMunicipality(value)
    { 
        select = document.getElementById('municipality');
        select.options.length = 1;
        const url = {!! '"'.url('api/locations?district='). '"'!!}+value;
        $.get(url,function(data,status){
            for (var i = 0; i < data.data.length; i++) {
                var option = document.createElement("option");
                option.value = data.data[i].local_level_en;
                option.text = data.data[i].local_level_en;
                console.log(option.text);
                select.appendChild(option);
            }           
        });
    }
    </script> 
@endsection
