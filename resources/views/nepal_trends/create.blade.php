@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Nepal Trend
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'nepalTrends.store']) !!}

                        @include('nepal_trends.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
