
<!-- Tested Total Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tested_total', 'Tested Total:') !!}
    {!! Form::number('tested_total', null, ['class' => 'form-control']) !!}
</div>

<!-- rdt Field -->
<div class="form-group col-sm-6">
    {!! Form::label('rdt', 'RDT:') !!}
    {!! Form::number('rdt', null, ['class' => 'form-control']) !!}
</div>

<!-- Tested Negative Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tested_negative', 'Tested Negative:') !!}
    {!! Form::number('tested_negative', null, ['class' => 'form-control']) !!}
</div>

<!-- Tested Positive Field -->
<div class="form-group col-sm-6">
    {!! Form::label('tested_positive', 'Tested Positive:') !!}
    {!! Form::number('tested_positive', null, ['class' => 'form-control']) !!}
</div>

<!-- Quarantine Field -->
<div class="form-group col-sm-6">
    {!! Form::label('quarantine', 'In Quarantine:') !!}
    {!! Form::number('quarantine', null, ['class' => 'form-control']) !!}
</div>
<!-- In Isolation Field -->
<div class="form-group col-sm-6">
    {!! Form::label('in_isolation', 'In Isolation:') !!}
    {!! Form::number('in_isolation', null, ['class' => 'form-control']) !!}
</div>

<!-- Recovered Field -->
<div class="form-group col-sm-6">
    {!! Form::label('recovered', 'Recovered:') !!}
    {!! Form::number('recovered', null, ['class' => 'form-control']) !!}
</div>
<!-- In recured Field -->
<div class="form-group col-sm-6">
    {!! Form::label('recured', 'Recured:') !!}
    {!! Form::number('recured', null, ['class' => 'form-control']) !!}
</div>

<!-- Critical Field -->
<div class="form-group col-sm-6">
    {!! Form::label('critical', 'Critical:') !!}
    {!! Form::number('critical', null, ['class' => 'form-control']) !!}
</div>
<!-- Deaths Field -->
<div class="form-group col-sm-6">
    {!! Form::label('deaths', 'Deaths:') !!}
    {!! Form::number('deaths', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('nepalTrends.index') }}" class="btn btn-default">Cancel</a>
</div>
