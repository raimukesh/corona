<div class="table-responsive">
    <table class="table" id="nepalTrends-table">
        <thead>
            <tr>
                <th>PCR Tested</th>
                <th>RDT Tested</th>
                <th>Tested Negative</th>
                <th>Tested Positive</th>
        <th>In Quarantine</th>
        <th>In Isolation</th>
        <th>Recovered</th>
        <th>Deaths</th>
        <th>Recured</th>
        <th>Critical</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($nepalTrends as $nepalTrend)
            <tr>
                <td>{{ $nepalTrend->tested_total }}</td>
                <td>{{ $nepalTrend->rdt }}</td>
                <td>{{ $nepalTrend->tested_negative }}</td>
                <td>{{ $nepalTrend->tested_positive }}</td>
            <td>{{ $nepalTrend->quarantine }}</td>
            <td>{{ $nepalTrend->in_isolation }}</td>
            <td>{{ $nepalTrend->recovered }}</td>
            <td>{{ $nepalTrend->deaths }}</td>
            <td>{{ $nepalTrend->recured }}</td>
            <td>{{ $nepalTrend->critical }}</td>
                <td>
                    {!! Form::open(['route' => ['nepalTrends.destroy', $nepalTrend->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('nepalTrends.show', [$nepalTrend->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('nepalTrends.edit', [$nepalTrend->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
