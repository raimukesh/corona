<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $nepalTrend->id }}</p>
</div>

<!-- Tested Positive Field -->
<div class="form-group">
    {!! Form::label('tested_positive', 'Tested Positive:') !!}
    <p>{{ $nepalTrend->tested_positive }}</p>
</div>

<!-- Tested Negative Field -->
<div class="form-group">
    {!! Form::label('tested_negative', 'Tested Negative:') !!}
    <p>{{ $nepalTrend->tested_negative }}</p>
</div>

<!-- Tested Total Field -->
<div class="form-group">
    {!! Form::label('tested_total', 'Tested Total:') !!}
    <p>{{ $nepalTrend->tested_total }}</p>
</div>

<!-- In Isolation Field -->
<div class="form-group">
    {!! Form::label('in_isolation', 'In Isolation:') !!}
    <p>{{ $nepalTrend->in_isolation }}</p>
</div>

<!-- Deaths Field -->
<div class="form-group">
    {!! Form::label('deaths', 'Deaths:') !!}
    <p>{{ $nepalTrend->deaths }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $nepalTrend->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $nepalTrend->updated_at }}</p>
</div>

