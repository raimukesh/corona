@extends('layouts.app')
@section('css')
<link href='https://api.mapbox.com/mapbox.js/v3.2.1/mapbox.css' rel='stylesheet' />
<style>
    body {
        margin: 0;
        padding: 0;
    }

    /* #map {
        position: absolute;
        top: 0;
        bottom: 0;
        width: 100%;
    } */

    .info {
        position: absolute;
        top: 10px;
        right: 10px;
    }

    .info div {
        background: #fff;
        padding: 10px;
        border-radius: 3px;
    }
</style>
@endsection

@section('content')
<div id='map' class="map" style="height: 750px"></div>
<div id='info' style="margin-top:40px" class='info'></div>
@endsection
@section('scripts')
<script src='https://api.mapbox.com/mapbox.js/v3.2.1/mapbox.js'></script>
<script>
    let individuals = {!! $individuals !!}
    L.mapbox.accessToken = 'pk.eyJ1Ijoic2FuYW1zaHJlc3RoYTEyMyIsImEiOiJjanpqcDhwaG0wNjNxM291aXZybjd3eng2In0.KOeiGVngAUlaFdVxsKFaNg';

    var info = document.getElementById('info');

    let setViewCoordinate = [27.7172, 85.3240];
    url = new URL(window.location.href);
    if (url.searchParams.get('lat')) {
        setViewCoordinate[0] = url.searchParams.get('lat');
    }
    if (url.searchParams.get('lng')) {
        setViewCoordinate[1] = url.searchParams.get('lng');
    }

    var map = L.mapbox.map('map')
        .setView(setViewCoordinate, 13)
        .addLayer(L.mapbox.styleLayer('mapbox://styles/mapbox/streets-v11'));
    var myLayer = L.mapbox.featureLayer().addTo(map);

    let geoJson = {
        type: 'Feature',
        features: []
    };

console.log(individuals)
    individuals.forEach((val) => {
        if (val.name) {
            console.log(val.status)
            if (val.status == 1) {
                markerColor = '#3dff03';
            } else if (val.status == 2) {
                markerColor = '#39bd11';
            } else if (val.status == 3) {
                markerColor = '#ffdf0d';;
            } else if (val.status == 4) {
                markerColor = '#ffa20d';
            } else if (val.status == 5) {
                markerColor = '#ff0d0d';
            } else {
                markerColor = '#000000';
            }
            var feature = {
                "type": "Feature",
                "properties": {
                    "id": val.id,
                    "name": val.name,
                    "phone": val.phone,
                    "iconSize": [15, 15],
                    "address": val.address,
                    icon: {
                        className: '', // class name to style
                        html: '<div style="height: 25px;width: 25px;border-radius: 50% !important; background-color: ' + markerColor + '">' +
                            '<p style="padding-left:8px;">' + val.status + '</p></div>',
                        iconSize: null // size of icon, use null to set the size in CSS
                    }
                },
                "geometry": {
                    "type": "Point",
                    "coordinates": [val.longitude, val.latitude]
                }
            }
            geoJson.features.push(feature)
        }

    });
    myLayer.on('layeradd', function(e) {
        var marker = e.layer,
            feature = marker.feature;
        marker.setIcon(L.divIcon(feature.properties.icon));
    });
    myLayer.setGeoJSON(geoJson);

    // Listen for individual marker hover.
    myLayer.on('mousemove', function(e) {
        // Force the popup closed.
        e.layer.closePopup();
        var feature = e.layer.feature;
        var content = '<div><strong>Individual Name: ' + feature.properties.name + '</strong><br>' +
            '<label>Contact Number: ' + feature.properties.phone + '</label><br>' +
            '<label>Address: ' + feature.properties.address + '</label></div>';

        info.innerHTML = content;
    });
    // Listen for individual marker click.
    myLayer.on('click', function(e) {
        // Force the popup closed.
        var feature = e.layer.feature;
        let now = '{!! \Carbon\Carbon::now() !!}';
        window.location.href = "/corona/individuals/" + feature.properties.id;
    });
    // Clear the tooltip when map is clicked.
    map.on('move', empty);
    // Trigger empty contents when the script
    // has loaded on the page.
    empty();

    function empty() {
        info.innerHTML = '<div><strong>Click a marker</strong></div>';
    }
</script>
@endsection