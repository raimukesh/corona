@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            User
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    <img style="border-radius:50%;border:5px solid black" src="{{asset('images/profile/'.Auth::user()->logo)}}" height="200px", width="200px" class="user-image"
                        alt="User Image" />
                </div>
                <div class="row" style="padding-left: 20px">
                    <!-- Name Field -->
                    <div class="col-md-6">
                        {!! Form::label('name', 'Name:') !!}
                        <p>{{ $user->name }}</p>
                    </div>

                    <!-- Phone Field -->
                    <div class="col-md-6">
                        {!! Form::label('phone', 'Phone:') !!}
                        <p>{{ $user->phone }}</p>
                    </div>

                    <!-- Ward Field -->
                    <div class="col-md-6">
                        {!! Form::label('ward', 'Ward:') !!}
                        <p>{{ $user->ward }}</p>
                    </div>

                    <!-- Municipality Field -->
                    <div class="col-md-6">
                        {!! Form::label('municipality', 'Municipality:') !!}
                        <p>{{ $user->municipality }}</p>
                    </div>
                    <!-- District Field -->
                    <div class="col-md-6">
                        {!! Form::label('district', 'District:') !!}
                        <p>{{ $user->district }}</p>
                    </div>

                    <!-- Province Field -->
                    <div class="col-md-6">
                        {!! Form::label('province', 'Province:') !!}
                        <p>{{ $user->province }}</p>
                    </div>

                    <!-- Address Field -->
                    <div class="col-md-6">
                        {!! Form::label('address', 'Address:') !!}
                        <p>{{ $user->address }}</p>
                    </div>

                    <!-- Email Field -->
                    <div class="col-md-6">
                        {!! Form::label('email', 'Email:') !!}
                        <p>{{ $user->email }}</p>
                    </div>

                    <a href="{{ route('home') }}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
