@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Server Number
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($serverNumber, ['route' => ['serverNumbers.update', $serverNumber->id], 'method' => 'patch']) !!}

                        @include('server_numbers.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection