<!-- User Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('individual_id', 'Individual Id:') !!}
    {!! Form::text('individual_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Rssi Field -->
<div class="form-group col-sm-6">
    {!! Form::label('rssi', 'Rssi:') !!}
    {!! Form::text('rssi', null, ['class' => 'form-control']) !!}
</div>

<!-- Duration Field -->
<div class="form-group col-sm-6">
    {!! Form::label('duration', 'Duration:') !!}
    {!! Form::text('duration', null, ['class' => 'form-control']) !!}
</div>

<!-- Distance Field -->
<div class="form-group col-sm-6">
    {!! Form::label('distance', 'Distance:') !!}
    {!! Form::text('distance', null, ['class' => 'form-control']) !!}
</div>

<!-- Friend Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('friend_id', 'Friend Id:') !!}
    {!! Form::text('friend_id', null, ['class' => 'form-control']) !!}
</div>

<!-- Latitude Field -->
<div class="form-group col-sm-6">
    {!! Form::label('latitude', 'Latitude:') !!}
    {!! Form::text('latitude', null, ['class' => 'form-control']) !!}
</div>

<!-- Longitude Field -->
<div class="form-group col-sm-6">
    {!! Form::label('longitude', 'Longitude:') !!}
    {!! Form::text('longitude', null, ['class' => 'form-control']) !!}
</div>

<!-- Date Field -->
<div class="form-group col-sm-6">
    {!! Form::label('date', 'Date:') !!}
    {!! Form::text('date', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('meetings.index') }}" class="btn btn-default">Cancel</a>
</div>
