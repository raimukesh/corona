<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $meeting->id }}</p>
</div>

<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('individual_id', 'Individual Id:') !!}
    <p>{{ $meeting->individual_id }}</p>
</div>

<!-- Rssi Field -->
<div class="form-group">
    {!! Form::label('rssi', 'Rssi:') !!}
    <p>{{ $meeting->rssi }}</p>
</div>

<!-- Duration Field -->
<div class="form-group">
    {!! Form::label('duration', 'Duration:') !!}
    <p>{{ $meeting->duration }}</p>
</div>

<!-- Distance Field -->
<div class="form-group">
    {!! Form::label('distance', 'Distance:') !!}
    <p>{{ $meeting->distance }}</p>
</div>

<!-- Friend Id Field -->
<div class="form-group">
    {!! Form::label('friend_id', 'Friend Id:') !!}
    <p>{{ $meeting->friend_id }}</p>
</div>

<!-- Latitude Field -->
<div class="form-group">
    {!! Form::label('latitude', 'Latitude:') !!}
    <p>{{ $meeting->latitude }}</p>
</div>

<!-- Longitude Field -->
<div class="form-group">
    {!! Form::label('longitude', 'Longitude:') !!}
    <p>{{ $meeting->longitude }}</p>
</div>

<!-- Date Field -->
<div class="form-group">
    {!! Form::label('date', 'Date:') !!}
    <p>{{ $meeting->date }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $meeting->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $meeting->updated_at }}</p>
</div>

