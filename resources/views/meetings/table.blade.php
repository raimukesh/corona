<div class="table-responsive">
    <table class="table" id="meetings-table">
        <thead>
            <tr>
                <th>Individual Id</th>
        <th>Rssi</th>
        <th>Duration</th>
        <th>Distance</th>
        <th>Friend Id</th>
        <th>Latitude</th>
        <th>Longitude</th>
        <th>Date</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($meetings as $meeting)
            <tr>
                <td>{{ $meeting->individual_id }}</td>
            <td>{{ $meeting->rssi }}</td>
            <td>{{ $meeting->duration }}</td>
            <td>{{ $meeting->distance }}</td>
            <td>{{ $meeting->friend_id }}</td>
            <td>{{ $meeting->latitude }}</td>
            <td>{{ $meeting->longitude }}</td>
            <td>{{ $meeting->date }}</td>
                <td>
                    {!! Form::open(['route' => ['meetings.destroy', $meeting->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('meetings.show', [$meeting->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('meetings.edit', [$meeting->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
