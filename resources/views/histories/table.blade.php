<div class="table-responsive">
    <table class="table table-bordered" id="histories-table">
        <thead>
            <tr>
                <th>Date</th>
                <th style="width:15%">Symptoms</th>
                <th>Individual Details</th>
                <th>Review</th>
                <th>Importance</th>
                {{-- <th colspan="3">Action</th> --}}
            </tr>
        </thead>
        <tbody>
        @foreach($histories as $history)
            <tr>
                <td>{{$history->created_at}}</td>
                <td>
                    @foreach (json_decode($history->symptoms) as $key=>$symptom)
                     <li>{!! $key.": ".$symptom !!}</li>   
                    @endforeach
                </td>
                <td>
                    {{ $history->individual->name }}<br>
                    {{ $history->individual->municipality."-".$history->individual->ward }}<br>
                    {{ $history->individual->phone }}
                </td>
                <td>
                    @if(!empty(json_decode($history->review)))
                        @foreach (json_decode($history->review) as $review)
                            <p class="@if($review->approved == 1) text-success @endif">
                                {{ $loop->iteration }}). {{ $review->review }}. @if($review->approved == 1) <button class="btn btn-sm btn-success" title="Approved review."><i class="fa fa-thumbs-up"></i></button> @endif<br>
                                Review by: <b>{{ $review->doctor_name }}</b>
                            </p>
                        @endforeach
                    @else
                        <span class="text-danger">Not Reviewed yet.</span>
                    @endif
                </td>
                <td>
                    @if($history->importance == "Normal")
                        <button class="btn btn-xs btn-primary">Normal</button>
                    @elseif($history->importance == "Important")
                        <button class="btn btn-xs btn-danger">Important</button>
                    @else
                        <button class="btn btn-xs btn-secondary">Not-Important</button>
                    @endif
                </td>
                {{-- <td>
                    {!! Form::open(['route' => ['histories.destroy', $history->id], 'method' => 'delete']) !!}
                    <div class='btn-g roup'>
                        <a href="{{ route('histories.show', [$history->id]) }}" class='btn btn-default btn-xs' title="View History"><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('histories.edit', [$history->id]) }}" class='btn btn-default btn-xs' title="change Importance" ><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs','title'=>'Delete History', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td> --}}
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
