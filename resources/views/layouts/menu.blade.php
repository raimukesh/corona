@if (Gate::allows('isAdmin'))
<li class="{{ Request::is('users*') ? 'active' : '' }}">
    <a href="{{route('users.index')}}">
        <i class="fas fa-users-cog"></i>
        <span class="title">Users</span>
    </a>
</li>
@endif

<li class="{{ Request::is('home*') ? 'active' : '' }}">
    <a href="{{route('home')}}">
        <i class="fas fa-asterisk"></i>
        <span class="title">Home</span>
    </a>
</li>
<li class="{{ Request::is('device_location*') ? 'active' : '' }}">
    <a href="{{route('deviceLocation')}}">
        <i class="fas fa-map-marker"></i>
        <span class="title">Map</span>
    </a>
</li>

{{-- <li class="{{ Request::is('officials*') ? 'active' : '' }}">
    <a href="{{ route('officials.index') }}"><i class="fas fa-user"></i><span>Officials</span></a>
</li> --}}


<li class="treeview {{ Request::is('individuals*') ? 'active' : '' }}">
    <a href="#"><i class="fas fa-users"></i> <span>Patients</span>
        <span class="pull-right-container">
            <i class="fas fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu {{ Request::is('individuals*') ? 'show-menu' : '' }}">
        <li class="{{ Request::is('individuals/normal') ? 'active' : '' }}">
            <a href="{{ route('individuals.normal') }}">&nbspNormal Patients</a>
        </li>
        <li class="{{ Request::is('individuals/critical') ? 'active' : '' }}">
            <a href="{{ route('individuals.critical') }}">&nbspSuspected/Critical Patients</a>
        </li>
        <li class="{{ Request::is('individuals') ? 'active' : '' }}">
            <a href="{{ route('individuals.index') }}">&nbspAll</a>
        </li>
    </ul>
</li>

<li class="{{ Request::is('doctors*') ? 'active' : '' }}">
    <a href="{{ route('doctors.index') }}"><i class="fas fa-user-md"></i><span>Doctors</span></a>
</li>


<li class="{{ Request::is('symptoms*') ? 'active' : '' }}">
    <a href="{{ route('symptoms.index') }}"><i class="fas fa-thermometer"></i><span>Symptoms</span></a>
</li>

<li class="{{ Request::is('histories*') ? 'active' : '' }}">
    <a href="{{ route('histories.index') }}"><i class="fas fa-history"></i><span>Histories</span></a>
</li>

<li class="{{ Request::is('news*') ? 'active' : '' }}">
    <a href="{{ route('news.index') }}"><i class="fas fa-newspaper"></i><span>News</span></a>
</li>

<li class="{{ Request::is('emergencyNumbers*') ? 'active' : '' }}">
    <a href="{{ route('emergencyNumbers.index') }}"><i class="fas fa-phone"></i><span>Emergency Numbers</span></a>
</li>

<li class="{{ Request::is('locations*') ? 'active' : '' }}">
    <a href="{{ route('locations.index') }}"><i class="fas fa-globe"></i><span>Locations</span></a>
</li>

<li class="{{ Request::is('announcements*') ? 'active' : '' }}">
    <a href="{{ route('announcements.index') }}"><i class="fas fa-bullhorn"></i><span>Announcements</span></a>
</li>

<li class="{{ Request::is('safeties*') ? 'active' : '' }}">
    <a href="{{ route('safeties.index') }}"><i class="fas fa-shield-alt"></i><span>Safety Tips</span></a>
</li>

<li class="{{ Request::is('nepalTrends*') ? 'active' : '' }}">
    <a href="{{ route('nepalTrends.index') }}"><i class="fas fa-chart-line"></i><span>Nepal Trends</span></a>
</li>

<li class="{{ Request::is('meetings*') ? 'active' : '' }}">
    <a href="{{ route('meetings.index') }}"><i class="fas fa-handshake"></i><span>Meetings</span></a>
</li>

<li class="{{ Request::is('serverNumbers*') ? 'active' : '' }}">
    <a href="{{ route('serverNumbers.index') }}"><i class="fas fa-hashtag"></i><span>Server Numbers</span></a>
</li>

<li class="{{ Request::is('dangerZones*') ? 'active' : '' }}">
    <a href="{{ route('dangerZones.index') }}"><i class="fas fa-exclamation-triangle"></i><span>Danger Zones</span></a>
</li>

