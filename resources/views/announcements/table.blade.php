<div class="table-responsive">
    <table class="table" id="announcements-table">
        <thead>
            <tr>
                <th>Title</th>
                <th>Thumbnail</th>
        <th>Description</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($announcements as $announcement)
            <tr>
                <td>{{ $announcement->title }}</td>
                <td><img height="100px" width="100px" src="{{asset($announcement->thumbnail)}}" alt=""></td>
            <td>{{ $announcement->description }}</td>
                <td>
                    {!! Form::open(['route' => ['announcements.destroy', $announcement->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('announcements.show', [$announcement->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('announcements.edit', [$announcement->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
