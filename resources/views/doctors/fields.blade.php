<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Phone Field -->
<div class="form-group col-sm-6">
    {!! Form::label('phone', 'Phone:') !!}
    {!! Form::text('phone', null, ['class' => 'form-control']) !!}
</div>

<!-- Ward Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ward', 'Ward:') !!}
    {!! Form::text('ward', null, ['class' => 'form-control']) !!}
</div>

<!-- Municipality Field -->
<div class="form-group col-sm-6">
    {!! Form::label('municipality', 'Municipality:') !!}
    {!! Form::text('municipality', null, ['class' => 'form-control']) !!}
</div>

<!-- Province Field -->
<div class="form-group col-sm-6">
    {!! Form::label('province', 'Province:') !!}
    {!! Form::text('province', null, ['class' => 'form-control']) !!}
</div>

<!-- Address Field -->
<div class="form-group col-sm-6">
    {!! Form::label('address', 'Address:') !!}
    {!! Form::text('address', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Password Field -->
<div class="form-group col-sm-6">
    {!! Form::label('password', 'Password:') !!}
    {!! Form::password('password', ['class' => 'form-control']) !!}
</div>

{{-- <!-- Nmc Card Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nmc_card', 'Nmc Card:') !!}
    {!! Form::file('nmc_card') !!}
</div> --}}
<div class="clearfix"></div>

<!-- Nmc Number Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nmc_number', 'Nmc Number:') !!}
    {!! Form::text('nmc_number', null, ['class' => 'form-control']) !!}
</div>

<!-- Speciality Field -->
<div class="form-group col-sm-6">
    {!! Form::label('speciality', 'Speciality:') !!}
    {!! Form::text('speciality', null, ['class' => 'form-control']) !!}
</div>

<!-- Level Field -->
<div class="form-group col-sm-6">
    {!! Form::label('level', 'Level:') !!}
    {!! Form::select('level', array('Expert' => 'Expert', 'Normal' => 'Normal'), null,['class' => 'form-control']); !!}
</div>

<!-- Post Field -->
<div class="form-group col-sm-6">
    {!! Form::label('post', 'Post:') !!}
    {!! Form::select('post', $dr_post , null,['class' => 'form-control']); !!}
</div>

<!-- Email Verified At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email_verified_at', 'Email Verified At:') !!}
    {!! Form::date('email_verified_at', null, ['class' => 'form-control','id'=>'email_verified_at']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#email_verified_at').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush

<!-- Approved At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('approved_at', 'Approved At:') !!}
    {!! Form::date('approved_at', null, ['class' => 'form-control','id'=>'approved_at']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#approved_at').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('doctors.index') }}" class="btn btn-default">Cancel</a>
</div>
