<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Phone Field -->
<div class="form-group col-sm-6">
    {!! Form::label('phone', 'Phone:') !!}
    {!! Form::text('phone', null, ['class' => 'form-control']) !!}
</div>

<!-- Ward Field -->
<div class="form-group col-sm-6">
    {!! Form::label('ward', 'Ward:') !!}
    {!! Form::text('ward', null, ['class' => 'form-control']) !!}
</div>

<!-- Municipality Field -->
<div class="form-group col-sm-6">
    {!! Form::label('municipality', 'Municipality:') !!}
    {!! Form::text('municipality', null, ['class' => 'form-control']) !!}
</div>

<!-- Province Field -->
<div class="form-group col-sm-6">
    {!! Form::label('province', 'Province:') !!}
    {!! Form::text('province', null, ['class' => 'form-control']) !!}
</div>

<!-- Address Field -->
<div class="form-group col-sm-6">
    {!! Form::label('address', 'Address:') !!}
    {!! Form::text('address', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>


<div class="clearfix"></div>

<!-- Nmc Number Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nmc_number', 'Nmc Number:') !!}
    {!! Form::text('nmc_number', null, ['class' => 'form-control']) !!}
</div>

<!-- Speciality Field -->
<div class="form-group col-sm-6">
    {!! Form::label('speciality', 'Speciality:') !!}
    {!! Form::text('speciality', null, ['class' => 'form-control']) !!}
</div>

<!-- Rating Field -->
<div class="form-group col-sm-6">
    {!! Form::label('rating', 'Rating:') !!}
    {!! Form::text('rating', null, ['class' => 'form-control']) !!}
</div>

<!-- Level Field -->
<div class="form-group col-sm-6">
    {!! Form::label('level', 'Level:') !!}
    {!! Form::select('level', array('Expert' => 'Expert', 'Normal' => 'Normal'), null,['class' => 'form-control']) !!}
</div>

<!-- Post Field -->
<div class="form-group col-sm-6">
    {!! Form::label('post', 'Post:') !!}
    {!! Form::select('post', $dr_post , null,['class' => 'form-control']); !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('doctors.index') }}" class="btn btn-default">Cancel</a>
</div>
