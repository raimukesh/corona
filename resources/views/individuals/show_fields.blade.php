<div class="table-responsive">
    <table class="table" id="individuals-table">
        <thead>
            <tr>
                <th>Name</th>
                <th>Phone</th>
                <th>Municipality</th>
                <th>District</th>
                <th>Province</th>
                <th>Address</th>
                <th>Email</th>
                <th>Condition Rating</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    {{ $individual->name }}<br>
                    @if($individual->age)
                        <code class="text-primary" style="font-style: italic;">{{ $individual->gender.": ".$individual->age." years" }}</code>
                    @endif
                </td>
                <td>
                    {{ $individual->phone }}<br>
                    <code>{{ "PLUS CODE: ".$individual->plus_code }}</code>
                </td>
                <td>{{ $individual->municipality." -".$individual->ward }}</td>
                <td>{{ $individual->district }}</td>
                <td>{{ $individual->province }}</td>
                <td>{{ $individual->address }}</td>
                <td>
                    {{ $individual->email }}
                    @if($individual->email_verified_at)
                    <button class="btn btn-xs btn-success">
                        <i class="fa fa-check"> {{ Carbon\Carbon::parse($individual->email_verified_at)->diffForHumans() }}</i>
                    </button>
                    @else
                    <button class="btn btn-danger btn-xs">Not Verified</span>
                    @endif
                </td>
               
                <td>
                    @if($individual->status == 1)
                    {{ ($individual->status) }}<span class="text-success">->Normal/Stay safe</span>
                    @elseif($individual->status == 2)
                    {{ ($individual->status) }}<span class="text-primary">->Rest at Home</span>
                    @elseif($individual->status == 3)
                    {{ ($individual->status) }}<span class="text-default">->Normal medication/Rest</span>
                    @elseif($individual->status == 4)
                    {{ ($individual->status) }}<span class="text-warning">->Go to Medical Personnal</span>
                    @else
                    {{ ($individual->status) }}<span class="text-danger">->Immediatly go to Hospital</span>
                    @endif
                </td>
            </tr>
        </tbody>
    </table>
</div>
<h3 style="color:green">Patient History</h1>
<div class="table-responsive">
    <table class="table table-bordered" id="histories-table">
        <thead>
            <tr>
                <th>#</th>
                <th style="width:15%">Symptoms</th>
                <th>Review</th>
                <th>Created_at</th>
            </tr>
        </thead>
        <tbody>
        @foreach($individual->histories->sortByDesc('id') as $history)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>
                    @foreach (json_decode($history->symptoms) as $key=>$symptom)
                     <li class="badge badge-primary">{!! $key.": ".$symptom !!}</li>   
                    @endforeach
                </td>
                <td>
                    @foreach (json_decode($history->review) as $review)
                    <p class="@if($review->approved == 1) text-success @endif">
                        {{ $loop->iteration }}). {{ $review->review }}. @if($review->approved == 1) <button class="btn btn-sm btn-success" title="Approved review."><i class="fa fa-thumbs-up"></i></button> @endif<br>
                        Review by: <b>{{ $review->doctor_name }}
                        </p>

                    @endforeach
                </td>
            <td>{{$history->created_at}}</td>
               
            </tr>
        @endforeach
        </tbody>
    </table>
</div>