@extends('layouts.app')

@section('css')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
@endsection
@section('content')
    <section class="content-header">
        <h1 class="pull-left">
       Total: ({{$individuals->count()}})
        List of
            @if(Request::is('individuals/normal'))
                normal people
            @elseif(Request::is('individuals/critical'))
                suspicious or critical patients
            @elseif(Request::is('individuals'))
                all patients
            @endif
            @if(Auth::user()->level!= 'admin')
                of @php
                    $level = Auth::user()->level;
                @endphp
                {{$level ." " .Auth::user()->$level}}
            @endif
        </h1>
        {{-- <h1 class="pull-right">
           <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{{ route('individuals.create') }}">Add New</a>
        </h1> --}}
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                    @include('individuals.table')
            </div>
        </div>
        <div class="text-center">
        
        </div>
    </div>
@endsection
@section('scripts')
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script>$(document).ready(function() {
    $('#individuals-table').DataTable({
        "aaSorting": [],
    });
} );
</script>
@endsection

