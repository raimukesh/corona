@if (Gate::allows('isAdmin'))

<li class="{{ Request::is('home*') ? 'active' : '' }}">
    <a href="{{route('home')}}">
        <i class="fa fa-asterisk"></i>
        <span class="title">Home</span>
    </a>
</li>
<li class="{{ Request::is('device_location*') ? 'active' : '' }}">
    <a href="{{route('deviceLocation')}}">
        <i class="fa fa-map-marker"></i>
        <span class="title">Map</span>
    </a>
</li>

@endif
<li class="{{ Request::is('official/officials*') ? 'active' : '' }}">
    <a href="{{ route('official.officials.index') }}"><i class="fa fa-user"></i><span>Officials</span></a>
</li>


<li class="treeview {{ Request::is('official/individuals*') ? 'active' : '' }}">
    <a href="#"><i class="fa fa-users"></i> <span>Individuals</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu {{ Request::is('official/individuals*') ? 'show-menu' : '' }}">
        <li class="{{ Request::is('official/individuals/normal') ? 'active' : '' }}">
            <a href="{{ route('official.individuals.normal') }}">&nbspNormal Individuals</a>
        </li>
        <li class="{{ Request::is('official/individuals/critical') ? 'active' : '' }}">
            <a href="{{ route('official.individuals.critical') }}">&nbspSuspected/Critical Individuals</a>
        </li>
        <li class="{{ Request::is('official/individuals') ? 'active' : '' }}">
            <a href="{{ route('official.individuals.index') }}">&nbspAll</a>
        </li>
    </ul>
</li>

<li class="{{ Request::is('official/doctors*') ? 'active' : '' }}">
    <a href="{{ route('official.doctors.index') }}"><i class="fa fa-user-md"></i><span>Doctors</span></a>
</li>


<li class="{{ Request::is('official/symptoms*') ? 'active' : '' }}">
    <a href="{{ route('official.symptoms.index') }}"><i class="fa fa-thermometer-2"></i><span>Symptoms</span></a>
</li>

<li class="{{ Request::is('official/histories*') ? 'active' : '' }}">
    <a href="{{ route('official.histories.index') }}"><i class="fa fa-history"></i><span>Histories</span></a>
</li>

<li class="{{ Request::is('official/news*') ? 'active' : '' }}">
    <a href="{{ route('official.news.index') }}"><i class="fa fa-edit"></i><span>News</span></a>
</li>

<li class="{{ Request::is('official/emergencyNumbers*') ? 'active' : '' }}">
    <a href="{{ route('official.emergencyNumbers.index') }}"><i class="fa fa-phone"></i><span>Emergency Numbers</span></a>
</li>

{{-- <li class="{{ Request::is('official/locations*') ? 'active' : '' }}">
    <a href="{{ route('official.locations.index') }}"><i class="fa fa-globe"></i><span>Locations</span></a>
</li> --}}

<li class="{{ Request::is('official/announcements*') ? 'active' : '' }}">
    <a href="{{ route('official.announcements.index') }}"><i class="fa fa-bullhorn"></i><span>Announcements</span></a>
</li>

<li class="{{ Request::is('official/safeties*') ? 'active' : '' }}">
    <a href="{{ route('official.safeties.index') }}"><i class="fa fa-shield"></i><span>Safeties</span></a>
</li>

