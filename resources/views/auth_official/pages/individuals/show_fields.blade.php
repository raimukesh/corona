<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{{ $individual->name }}</p>
</div>

<!-- Phone Field -->
<div class="form-group">
    {!! Form::label('phone', 'Phone:') !!}
    <p>{{ $individual->phone }}</p>
</div>

<!-- Ward Field -->
<div class="form-group">
    {!! Form::label('ward', 'Ward:') !!}
    <p>{{ $individual->ward }}</p>
</div>

<!-- Municipality Field -->
<div class="form-group">
    {!! Form::label('municipality', 'Municipality:') !!}
    <p>{{ $individual->municipality }}</p>
</div>

<!-- Province Field -->
<div class="form-group">
    {!! Form::label('province', 'Province:') !!}
    <p>{{ $individual->province }}</p>
</div>

<!-- Address Field -->
<div class="form-group">
    {!! Form::label('address', 'Address:') !!}
    <p>{{ $individual->address }}</p>
</div>
<div class="form-group">
    {!! Form::label('pluse_code', 'Pluse Code:') !!}
    <p>{{ $individual->pluse_code }}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{{ $individual->email }}</p>
</div>


<div class="table-responsive">
    <table class="table table-bordered" id="histories-table">
        <thead>
            <tr>
                <th>#</th>
                <th style="width:15%">Symptoms</th>
                <th>Review</th>
                <th>Created_at</th>
            </tr>
        </thead>
        <tbody>
        @foreach($individual->histories->sortByDesc('id') as $history)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>
                    @foreach (json_decode($history->symptoms) as $key=>$symptom)
                     <li class="badge badge-primary">{!! $key.": ".$symptom !!}</li>   
                    @endforeach
                </td>
                <td>
                    @foreach (json_decode($history->review) as $review)
                    <p class="@if($review->approved == 1) text-success @endif">
                        {{ $loop->iteration }}). {{ $review->review }}. @if($review->approved == 1) <button class="btn btn-sm btn-success" title="Approved review."><i class="fa fa-thumbs-up"></i></button> @endif<br>
                        Review by: <b>{{ $review->doctor_name }}
                        </p>

                    @endforeach
                </td>
            <td>{{$history->created_at}}</td>
               
            </tr>
        @endforeach
        </tbody>
    </table>
</div>