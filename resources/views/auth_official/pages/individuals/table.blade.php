<div class="table-responsive">
    <table class="table" id="individuals-table">
        <thead>
            <tr>
                <th>S.N.</th>
                <th>Name</th>
                <th>Phone</th>
                <th>Municipality</th>
                <th>District</th>
                <th>Province</th>
                <th>Address</th>
                <th>Email</th>
                <th>Condition Rating</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($individuals as $individual)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>
                    {{ $individual->name }}<br>
                    @if($individual->age)
                        <code class="text-primary" style="font-style: italic;">{{ $individual->gender.": ".$individual->age." years" }}</code>
                    @endif
                </td>
                <td>
                    {{ $individual->phone }}<br>
                    <code>{{ "PLUS CODE: ".$individual->plus_code }}</code>
                </td>
                <td>{{ $individual->municipality." -".$individual->ward }}</td>
                <td>{{ $individual->district }}</td>
                <td>{{ $individual->province }}</td>
                <td>{{ $individual->address }}</td>
                <td>
                    {{ $individual->email }}
                    @if($individual->email_verified_at)
                    <button class="btn btn-xs btn-success">
                        <i class="fa fa-check"> {{ Carbon\Carbon::parse($individual->email_verified_at)->diffForHumans() }}</i>
                    </button>
                    @else
                    <button class="btn btn-danger btn-xs">Not Verified</span>
                    @endif
                </td>
               
                <td>
                    @if($individual->status == 1)
                    {{ ($individual->status) }}<span class="text-success">->Normal/Stay safe</span>
                    @elseif($individual->status == 2)
                    {{ ($individual->status) }}<span class="text-primary">->Rest at Home</span>
                    @elseif($individual->status == 3)
                    {{ ($individual->status) }}<span class="text-default">->Normal medication/Rest</span>
                    @elseif($individual->status == 4)
                    {{ ($individual->status) }}<span class="text-warning">->Go to Medical Personnal</span>
                    @else
                    {{ ($individual->status) }}<span class="text-danger">->Immediatly go to Hospital</span>
                    @endif
                </td>
                <td>
                    {!! Form::open(['route' => ['individuals.destroy', $individual->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('individuals.show', [$individual->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('individuals.edit', [$individual->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
