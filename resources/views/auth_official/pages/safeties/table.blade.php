<div class="table-responsive">
    <table class="table" id="safeties-table">
        <thead>
            <tr>
                <th>Title</th>
        <th>Description</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($safeties as $safety)
            <tr>
                <td>{{ $safety->title }}</td>
            <td>{!! $safety->description !!}</td>
                <td>
                    {!! Form::open(['route' => ['safeties.destroy', $safety->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('safeties.show', [$safety->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('safeties.edit', [$safety->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
