<!-- Symptoms Field -->
<div class="form-group col-sm-6">
    {!! Form::label('symptoms', 'Symptoms:') !!}
    {!! Form::text('symptoms', null, ['class' => 'form-control', 'disabled']) !!}
</div>

<!-- Level Field -->
<div class="form-group col-sm-6">
    {!! Form::label('importance', 'Importance:') !!}
    {!! Form::select('importance', array('Normal' => 'Normal', 'Important' => 'Important', 'not-Important'=> 'Not-Important'), 'Normal',['class' => 'form-control']); !!}
</div>

<!-- Individual Id Field -->
{{-- <div class="form-group col-sm-6">
    {!! Form::label('individual_id', 'Individual Id:') !!}
    {!! Form::text('individual_id', null, ['class' => 'form-control']) !!}
</div> --}}

<!-- Review Field -->
<div class="form-group col-sm-6">
    {!! Form::label('review', 'Review:') !!}
    {!! Form::text('review', null, ['class' => 'form-control', 'disabled']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('histories.index') }}" class="btn btn-default">Cancel</a>
</div>
