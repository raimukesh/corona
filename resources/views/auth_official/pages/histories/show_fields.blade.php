{{-- <!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $history->id }}</p>
</div> --}}
<!-- Importance Field -->
<div class="form-group">
    {!! Form::label('Importance', 'Importance:') !!}
    <p>{{ $history->importance }}</p>
</div>

<!-- Symptoms Field -->
<div class="form-group">
    {!! Form::label('symptoms', 'Symptoms:') !!}
    <pre>{{ $history->symptoms }}</pre>
</div>

<!-- Individual Id Field -->
<div class="form-group">
    {!! Form::label('individual', 'Individual Name:') !!}
    <p>{{ $history->individual->name }}</p>
</div>

<!-- Review Field -->
<div class="form-group">
    {!! Form::label('review', 'Review:') !!}
    <br><code>{{ $history->review }}</code>
</div>

<!-- Created At Field -->
{{-- <div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $history->created_at }}</p>
</div> --}}

<!-- Updated At Field -->
{{-- <div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $history->updated_at }}</p>
</div> --}}

