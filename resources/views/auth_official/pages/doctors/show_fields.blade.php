<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{{ $doctor->id }}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{{ $doctor->name }}</p>
</div>

<!-- Phone Field -->
<div class="form-group">
    {!! Form::label('phone', 'Phone:') !!}
    <p>{{ $doctor->phone }}</p>
</div>

<!-- Ward Field -->
<div class="form-group">
    {!! Form::label('ward', 'Ward:') !!}
    <p>{{ $doctor->ward }}</p>
</div>

<!-- Municipality Field -->
<div class="form-group">
    {!! Form::label('municipality', 'Municipality:') !!}
    <p>{{ $doctor->municipality }}</p>
</div>

<!-- Province Field -->
<div class="form-group">
    {!! Form::label('province', 'Province:') !!}
    <p>{{ $doctor->province }}</p>
</div>

<!-- Address Field -->
<div class="form-group">
    {!! Form::label('address', 'Address:') !!}
    <p>{{ $doctor->address }}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{{ $doctor->email }}</p>
</div>

<!-- Password Field -->
{{-- <div class="form-group">
    {!! Form::label('password', 'Password:') !!}
    <p>{{ $doctor->password }}</p>
</div> --}}


<!-- Nmc Number Field -->
<div class="form-group">
    {!! Form::label('nmc_number', 'Nmc Number:') !!}
    <p>{{ $doctor->nmc_number }}</p>
</div>

<!-- Doctor Level -->
<div class="form-group">
    {!! Form::label('level', 'Level:') !!}
    <p>{{ $doctor->level }}</p>
</div>

<!-- Speciality Field -->
<div class="form-group">
    {!! Form::label('speciality', 'Speciality:') !!}
    <p>{{ $doctor->speciality }}</p>
</div>

<!-- Email Verified At Field -->
<div class="form-group">
    {!! Form::label('email_verified_at', 'Email Verified At:') !!}
    <p>{{ $doctor->email_verified_at }}</p>
</div>

<!-- Approved At Field -->
<div class="form-group">
    {!! Form::label('approved_at', 'Approved At:') !!}
    <p>{{ $doctor->approved_at }}</p>
</div>

<!-- Created At Field -->
{{-- <div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $doctor->created_at }}</p>
</div> --}}

<!-- Updated At Field -->
{{-- <div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $doctor->updated_at }}</p>
</div> --}}

<!-- Api Token Field -->
{{-- <div class="form-group">
    {!! Form::label('api_token', 'Api Token:') !!}
    <p>{{ $doctor->api_token }}</p>
</div> --}}

