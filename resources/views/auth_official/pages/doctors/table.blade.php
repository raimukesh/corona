<div class="table-responsive">
    <table class="table" id="doctors-table">
        <thead>
            <tr>
                <th>S.N.</th>
                <th>Name</th>
                <th>Degree</th>
                <th>Municipality</th>
                {{-- <th>Province</th> --}}
                <th>Address</th>
                <th>Email</th>
                <th>Nmc Number</th>
                <th>Speciality</th>
                <th>Level</th>
                {{-- <th>Email Verified At</th> --}}
                {{-- <th>Approved At</th> --}}
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($doctors as $doctor)
            <tr>
                <td>{{ $loop->iteration }} </td>
                <td>
                    {{ $doctor->name }}<br>
                    @if($doctor->phone)
                        <code>{{ $doctor->phone }}</code>
                    @endif
                </td>
                <td>{{ $doctor->degree }}</td>
                <td>{{ $doctor->municipality."-".$doctor->ward }}</td>
                {{-- <td>{{ $doctor->province }}</td> --}}
                <td>{{ $doctor->address }}</td>
                <td>{{ $doctor->email }}</td>
                <td>{{ $doctor->nmc_number }}</td>
                <td>{{ $doctor->speciality }}</td>
                <td>{{ $doctor->level }}</td>
                {{-- <td>{{ $doctor->email_verified_at }}</td> --}}
                {{-- <td>{{ $doctor->approved_at }}</td> --}}
                <td>
                    {!! Form::open(['route' => ['doctors.destroy', $doctor->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('doctors.show', [$doctor->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('doctors.edit', [$doctor->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
