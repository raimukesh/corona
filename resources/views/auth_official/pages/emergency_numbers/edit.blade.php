@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Emergency Number
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($emergencyNumber, ['route' => ['emergencyNumbers.update', $emergencyNumber->id], 'method' => 'patch']) !!}

                        @include('emergency_numbers.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection