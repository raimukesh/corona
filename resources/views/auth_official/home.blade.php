@extends('auth_official.layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <a href="{{route('official.individuals.index')}}" class="col-md-3" style="background:#51515c; border:5px solid white">
                <div class="text-center"> <b style="font-size:24px;color:white">Patients</b> </div>
            <div><img style="padding:50px" class="img-responsive" src="{{asset('images/icons/birami.png')}}" alt=""></div>
            </a>
        <a href="{{route('official.doctors.index')}}" class="col-md-3" style="background:#51515c; border:5px solid white">
                <div class="text-center"> <b style="font-size:24px;color:white">Doctor</b> </div>
            <div><img style="padding:50px" class="img-responsive" src="{{asset('images/icons/doctor.png')}}" alt=""></div>
        </a>
        {{-- <a href="{{route('official.deviceLocation')}}" class="col-md-3" style="background:#51515c; border:5px solid white">
                <div class="text-center"> <b style="font-size:24px;color:white">Map</b> </div>
            <div><img style="padding:50px" class="img-responsive" src="{{asset('images/icons/map.png')}}" alt=""></div>
        </a> --}}
        <a href="{{route('official.emergencyNumbers.index')}}" class="col-md-3" style="background:#51515c; border:5px solid white">
                <div class="text-center"> <b style="font-size:24px;color:white">Emergency Number</b> </div>
            <div><img style="padding:50px" class="img-responsive" src="{{asset('images/icons/number.png')}}" alt=""></div>
        </a>
        
        <a href="{{route('official.announcements.index')}}" class="col-md-3" style="background:#51515c; border:5px solid white">
            <div class="text-center"> <b style="font-size:24px;color:white">Announcements</b> </div>
        <div><img style="padding:50px" class="img-responsive" src="{{asset('images/icons/announcements.png')}}" alt=""></div>
        </a>
        <a href="{{route('official.safeties.index')}}" class="col-md-3" style="background:#51515c; border:5px solid white">
            <div class="text-center"> <b style="font-size:24px;color:white">Safeties</b> </div>
        <div><img style="padding:50px" class="img-responsive" src="{{asset('images/icons/safety_tips.png')}}" alt=""></div>
        </a>
        <a href="{{route('official.news.index')}}" class="col-md-3" style="background:#51515c; border:5px solid white">
            <div class="text-center"> <b style="font-size:24px;color:white">News</b> </div>
        <div><img style="padding:50px" class="img-responsive" src="{{asset('images/icons/news.png')}}" alt=""></div>
        </a>

    </div>
</div>
@endsection
