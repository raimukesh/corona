<div class="table-responsive">
    <table class="table" id="symptoms-table">
        <thead>
            <tr>
                <th>S.N.</th>
                <th>Name</th>
                <th>Name_in_Nepali</th>
                <th>Color</th>
                <th>Icon</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($symptoms as $symptom)
            <tr>
                <td>{{ $loop->iteration }} </td>
                <td>{{ $symptom->name }}</td>
                <td>{{ $symptom->name_in_nepali }}</td>
                <td><img src="{{ $symptom->icon }}" alt=""></td>
                <td><span class="badge"  style="background:{{ $symptom->color }}">{{ $symptom->color }}</span></td>
                <td>
                    {!! Form::open(['route' => ['symptoms.destroy', $symptom->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('symptoms.show', [$symptom->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('symptoms.edit', [$symptom->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
